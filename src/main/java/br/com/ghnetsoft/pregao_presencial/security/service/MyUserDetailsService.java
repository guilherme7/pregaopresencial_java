package br.com.ghnetsoft.pregao_presencial.security.service;

import java.util.Set;
import java.util.stream.Collectors;

import org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.ghnetsoft.pregao_presencial.core.exception.UsuarioNaoAtivoException;
import br.com.ghnetsoft.pregao_presencial.core.usuario.Usuario;
import br.com.ghnetsoft.pregao_presencial.core.usuario.UsuarioConsultaService;
import lombok.extern.apachecommons.CommonsLog;

/**
 * @author Guilherme C Lopes
 *
 * @version Service para buscar o usuário deste sistema ao logar
 */
@CommonsLog
@Service
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	private UsuarioConsultaService service;

	/**
	 * Sobescreve o metodo loadUserByUsername
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		log.debug("Authenticating {}" + username);
		if (new EmailValidator().isValid(username, null)) {
			return service.buscarPeloLogin(username).map(usuario -> criandoUsuarioSeguroSpring(username, usuario))
					.orElseThrow(() -> new UsernameNotFoundException(
							"Email " + username + " não existe em nossa base de dados !"));
		}
		return service.buscarPeloLogin(username).map(usuario -> criandoUsuarioSeguroSpring(username, usuario))
				.orElseThrow(() -> new UsernameNotFoundException(
						"Usuário " + username + " não existe em nossa base de dados !"));
	}

	private User criandoUsuarioSeguroSpring(String username, Usuario usuario) {
		if (!usuario.isActivated()) {
			throw new UsuarioNaoAtivoException("Usuário " + username + " não está ativado !");
		}
		Set<SimpleGrantedAuthority> roles = usuario.getUsuariosRoles().stream()
				.map(usuarioRole -> new SimpleGrantedAuthority(usuarioRole.getRole().getRole()))
				.collect(Collectors.toSet());
		return new User(usuario.getLogin(), usuario.getSenha(), roles);
	}
}
