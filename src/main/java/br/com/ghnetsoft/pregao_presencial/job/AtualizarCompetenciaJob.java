package br.com.ghnetsoft.pregao_presencial.job;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.apachecommons.CommonsLog;

@CommonsLog
@Component
@EnableScheduling
public class AtualizarCompetenciaJob {

	private static final String MUDANCA_LINHA = "--------------------------------";

	private static final String INICIO = "--> ";

	
	@Scheduled(cron = "${cron.expression.atualizacompetencia}", zone = "America/Sao_Paulo")
	public void scheduleTaskWithFixedRate() throws InterruptedException {
		atualizaCompetencia();
	}

	public void atualizaCompetencia() throws InterruptedException {
		/*log.info(INICIO + "Inicio da atualiação de competencia");
		Collection<Empresa> empresas = empresaConsultaService.buscarPorTodosAtivos();
		log.info(INICIO + "Total de empresas ativas: " + empresas.size());
		empresas.forEach(empresa -> {
			log.info(INICIO + "Empresa ativa: " + empresa.getCnpj() + " - " + empresa.getRazaoSocial());
			Collection<MesAno> mesAnos = mesAnoConsultaService.buscaTodasCompetenciasPorEmpresaAbertas(empresa);
			log.info(INICIO + "Total de competencias abertas: " + mesAnos.size());
			mesAnos.forEach(mesano -> {
				log.info(INICIO + "Competencia aberta - mes: " + mesano.getMes().name() + " / ano: " + mesano.getAno());
				mesano.setTipo(FECHAMENTO);
				log.info(INICIO + "Atualiza competencia - mes: " + mesano.getMes().name() + " / ano: " + mesano.getAno()
						+ " - aberta e sendo fechada");
				service.salvar(mesano);
				log.info(INICIO + MUDANCA_LINHA);
			});
			if (mesAnos.size() > 0) {
				LocalDate data = DateUtil.obterDataComPrimeiroDiaMesSeguinte(new LocalDate());
				int mes = data.getMonthOfYear() - 1;
				int ano = data.getYear();
				log.info(INICIO + "Inicia competencia - mes: " + buscaMesEnum(mes) + " / ano: " + ano + " - aberta");
				MesAno mesano = MesAno.builder().empresa(empresa).tipo(ABERTURA).mes(MesEnum.buscaMesEnum(mes)).ano(ano)
						.build();
				AuditMetadata auditMetadata = AuditMetadata.builder().loginMovimentacao("JOB").build();
				mesano.setMetadadoAuditoria(auditMetadata);
				service.salvar(mesano);
				log.info(INICIO + "Atualiza competencia - mes: " + buscaMesEnum(mes) + " / ano: " + ano + " - salvar");
			}
			log.info(INICIO + MUDANCA_LINHA);
		});
		log.info(INICIO + "Fim da atualiação de competencia");*/
	}
}
