package br.com.ghnetsoft.pregao_presencial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * @author Guilherme C Lopes
 *
 * @version Configuracao dos spring boot
 */
@ComponentScan(basePackages = "br.com.ghnetsoft")
@SpringBootApplication
@EnableScheduling
public class PregaopresencialApplication {

	/**
	 * @version inicio do sistema
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(PregaopresencialApplication.class, args);
	}

	/**
	 * @version Corfilters
	 * 
	 * @return
	 */
	@Bean
	public CorsFilter corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("PUT");
		config.addAllowedMethod("POST");
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("DELETE");
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}
}
