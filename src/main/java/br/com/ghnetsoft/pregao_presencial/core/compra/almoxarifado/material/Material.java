package br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.material;

import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.classe.MaterialClasse;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_MATERIAL", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "FK_MATERIAL_CLASSE", "DS_CODIGO" }, name = "U1_TAB_MATERIAL") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_MATERIAL"),
				@Index(columnList = "FK_MATERIAL_CLASSE", name = "I2_TAB_MATERIAL"),
				@Index(columnList = "DS_CODIGO", name = "I3_TAB_MATERIAL") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class Material extends Principal {

	private static final long serialVersionUID = -5928396152937991784L;

	@Id
	@Column(name = "PK_MATERIAL")
	@SequenceGenerator(name = "SQ_MATERIAL", sequenceName = "SQ_MATERIAL", allocationSize = 1)
	@GeneratedValue(generator = "SQ_MATERIAL", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = MaterialClasse.class)
	@JoinColumn(name = "FK_MATERIAL_CLASSE", nullable = false, foreignKey = @ForeignKey(name = "FK_MATERIAL_CLASSE"))
	private MaterialClasse classe;
	@Column(name = "DS_CODIGO", nullable = false, length = 5)
	private String codigo;
	@Column(name = "DS_DESCRICAO", nullable = false)
	private String descricao;
}
