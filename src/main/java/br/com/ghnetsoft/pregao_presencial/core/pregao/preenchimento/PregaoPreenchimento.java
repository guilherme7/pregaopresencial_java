package br.com.ghnetsoft.pregao_presencial.core.pregao.preenchimento;

import java.math.BigDecimal;

/**
 * @author Guilherme C Lopes
 * 
 * @version Interface que preenche o valor de uma licitacao
 */
public interface PregaoPreenchimento {

	/**
	 * @version Metodo que preenche uma licitacao
	 * 
	 * @param numero
	 * @param ano
	 * @return
	 */
	String preenchimento(Integer numero, Integer ano);

	/**
	 * @version Metodo que preenche uma licitacao com o valor estimado
	 * 
	 * @param numero
	 * @param ano
	 * @param valorEstimado
	 * @return
	 */
	String preenchimentoComValor(Integer numero, Integer ano, BigDecimal valorEstimado);
}
