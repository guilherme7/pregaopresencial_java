package br.com.ghnetsoft.pregao_presencial.core.pregao.impl;

import static br.com.ghnetsoft.pregao_presencial.core.enuns.LicitacaoRegimeEnum.MENOR_PRECO_GLOBAL;
import static br.com.ghnetsoft.pregao_presencial.core.enuns.LicitacaoRegimeEnum.buscaLicitacaoRegimeEnum;
import static br.com.ghnetsoft.pregao_presencial.core.enuns.PregaoPregaoItemFasesEnum.buscaPregaoPregaoItemFasesEnum;
import static org.apache.commons.lang3.StringUtils.isEmpty;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import br.com.ghnetsoft.pregao_presencial.core.competencia.preenchimento.CompetenciaPreenchimento;
import br.com.ghnetsoft.pregao_presencial.core.competencia.resource.CompetenciaSimplesRetornoResource;
import br.com.ghnetsoft.pregao_presencial.core.entidade.preenchimento.EntidadePreenchimento;
import br.com.ghnetsoft.pregao_presencial.core.licitacao.preenchimento.LicitacaoPreenchimento;
import br.com.ghnetsoft.pregao_presencial.core.pregao.Pregao;
import br.com.ghnetsoft.pregao_presencial.core.pregao.PregaoConsultaService;
import br.com.ghnetsoft.pregao_presencial.core.pregao.resource.PregaoPesquisaEnvioResource;
import br.com.ghnetsoft.pregao_presencial.core.pregao.resource.PregaoPesquisaPaginacaoResource;
import br.com.ghnetsoft.pregao_presencial.core.pregao.resource.PregaoPesquisaRetornoResource;
import br.com.ghnetsoft.pregao_presencial.core.pregao.resource.PregaoSimplesRetornoResource;
import br.com.ghnetsoft.pregao_presencial.core.pregaoitem.PregaoItemConsultaService;
import br.com.ghnetsoft.principal.core.resorce.EnumResource;
import br.com.ghnetsoft.principal.core.service.PrincipalService;

@Service
public class PregaoConsultaServiceImpl extends PrincipalService implements PregaoConsultaService {

	private static final long serialVersionUID = 4933591516193144302L;

	@Autowired
	private PregaoRepository repository;
	@Autowired
	private LicitacaoPreenchimento licitacaoPreenchimento;
	@Autowired
	private EntidadePreenchimento entidadePreenchimento;
	@Autowired
	private PregaoPaginar paginar;
	@Autowired
	private CompetenciaPreenchimento competenciaPreenchimento;
	@Autowired
	private PregaoItemConsultaService pregaoItemConsultaService;

	@Override
	public Collection<PregaoSimplesRetornoResource> ativosPorUsuario(Long idUsuario) {
		Collection<Pregao> pregaoLista = repository.ativosPorUsuario(idUsuario);
		Collection<PregaoSimplesRetornoResource> retorno = new ArrayList<>();
		pregaoLista.forEach(pregao -> {
			PregaoSimplesRetornoResource resource = PregaoSimplesRetornoResource.builder()
					.pregaoEntidade(licitacaoPreenchimento.preenchimento(pregao.getLicitacao().getNumero(),
							pregao.getLicitacao().getAno()) + " - "
							+ entidadePreenchimento.preenchimento(pregao.getLicitacao().getEntidade().getCnpj(),
									pregao.getLicitacao().getEntidade().getRazaoSocial()))
					.build();
			resource.setId(pregao.getId());
			retorno.add(resource);
		});
		return retorno;
	}

	@Override
	public PregaoPesquisaPaginacaoResource paginacaoPregao(PregaoPesquisaEnvioResource resource) {
		PageRequest pageable = paginacao(resource);
		Page<Pregao> pregoes = repository.paginacao(resource.getIdEntidade(), resource.getIdPregao(),
				resource.getIdCompetenciaPregao(),
				isEmpty(resource.getFasePregao()) ? null : buscaPregaoPregaoItemFasesEnum(resource.getFasePregao()),
				isEmpty(resource.getRegime()) ? null : buscaLicitacaoRegimeEnum(resource.getRegime()),
				isEmpty(resource.getValorEstimado()) ? null
						: BigDecimal.valueOf(Double.valueOf(resource.getValorEstimado())),
				null // statusDoRegistro
				, resource.getIdCompetenciaLicitacao(), pageable);
		return PregaoPesquisaPaginacaoResource.builder().lista(resourceMapa(pregoes.getContent()))
				.paginacao(paginar.paginacao(resource, pregoes)).build();
	}

	private Collection<PregaoPesquisaRetornoResource> resourceMapa(Collection<Pregao> pregoes) {
		Collection<PregaoPesquisaRetornoResource> retorno = new ArrayList<>();
		pregoes.forEach(pregao -> {
			BigDecimal valorPorLance = null;
			Long quantidadeFornecedor = null;
			if (pregao.getLicitacao().getRegime().equals(MENOR_PRECO_GLOBAL)) {
				valorPorLance = pregao.getValorMinimoPorLance();
				quantidadeFornecedor = pregao.getQuantidadeFornecedor();
			}
			PregaoSimplesRetornoResource pregaoLicitacao = PregaoSimplesRetornoResource.builder()
					.pregaoEntidade(licitacaoPreenchimento.preenchimento(pregao.getLicitacao().getNumero(),
							pregao.getLicitacao().getAno()))
					.build();
			pregaoLicitacao.setId(pregao.getId());
			CompetenciaSimplesRetornoResource competencia = CompetenciaSimplesRetornoResource.builder()
					.mesAno(competenciaPreenchimento.preenchimento(pregao.getCompetencia().getMes().getDescricao(),
							pregao.getCompetencia().getAno()))
					.build();
			competencia.setId(pregao.getCompetencia().getId());
			PregaoPesquisaRetornoResource resource = PregaoPesquisaRetornoResource.builder()
					.abertura(pregao.getDataHoraAbertura()).competencia(competencia)
					.fase(EnumResource.builder().key(pregao.getFase().name()).texto(pregao.getFase().getDescricao())
							.build())
					.fechamento(pregao.getDataHoraFechou()).licitacao(pregaoLicitacao)
					.quantidadeFornecedor(quantidadeFornecedor)
					.regime(EnumResource.builder().key(pregao.getLicitacao().getRegime().name())
							.texto(pregao.getLicitacao().getRegime().getDescricao()).build())
					.valorEstimado(pregao.getLicitacao().getValorEstimado()).valorPorLance(valorPorLance)
					.quantidadeItens(pregaoItemConsultaService.contarQuantidadeItensPorPregao(pregao.getId())).build();
			resource.setId(pregao.getId());
			retorno.add(resource);
		});
		return retorno;
	}
}
