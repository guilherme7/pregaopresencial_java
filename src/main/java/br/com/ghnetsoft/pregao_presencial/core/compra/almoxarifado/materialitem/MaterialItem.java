package br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.materialitem;

import static javax.persistence.GenerationType.SEQUENCE;
import static javax.persistence.TemporalType.TIMESTAMP;
import static lombok.AccessLevel.PROTECTED;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.material.Material;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_MATERIAL_ITEM", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "DS_FEDERAL_SUPPLY", "FK_MATERIAL" }, name = "U1_TAB_MATERIAL_ITEM"),
		@UniqueConstraint(columnNames = { "DS_CODIGO", "FK_MATERIAL" }, name = "U2_TAB_MATERIAL_ITEM") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_MATERIAL_ITEM"),
				@Index(columnList = "FK_MATERIAL", name = "I2_TAB_MATERIAL_ITEM"),
				@Index(columnList = "DS_CODIGO", name = "I3_TAB_MATERIAL_ITEM"),
				@Index(columnList = "DS_NOME", name = "I4_TAB_MATERIAL_ITEM"),
				@Index(columnList = "DS_FEDERAL_SUPPLY", name = "I5_TAB_MATERIAL_ITEM") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class MaterialItem extends Principal {

	private static final long serialVersionUID = -764709956802815515L;

	@Id
	@Column(name = "PK_MATERIAL_ITEM")
	@SequenceGenerator(name = "SQ_MATERIAL_ITEM", sequenceName = "SQ_MATERIAL_ITEM", allocationSize = 1)
	@GeneratedValue(generator = "SQ_MATERIAL_ITEM", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Material.class)
	@JoinColumn(name = "FK_MATERIAL", nullable = false, foreignKey = @ForeignKey(name = "FK_MATERIAL"))
	private Material material;
	@Column(name = "DS_FEDERAL_SUPPLY", nullable = false, length = 20)
	private String federalSupply;
	@Column(name = "DS_NUMERO_IDENTIFICADOR", nullable = false, length = 2)
	private String numeroIdentificador;
	@Column(name = "DS_CODIGO", nullable = false, length = 10)
	private String codigo;
	@Column(name = "DS_NOME", nullable = false)
	private String nome;
	@Column(name = "DES_DESCRICAO", nullable = false, length = 5000)
	private String descricso;
	@Column(name = "NR_MINIMO", nullable = false, length = 25, precision = 2)
	private BigDecimal minimo;
	@Column(name = "NR_MAXIMO", nullable = false, length = 25, precision = 2)
	private BigDecimal maximo;
	@Temporal(TIMESTAMP)
	@Column(name = "TS_DATA_HORA_ULTIMA_COMPRA")
	private Date dataHoraUltimaCompra;
}
