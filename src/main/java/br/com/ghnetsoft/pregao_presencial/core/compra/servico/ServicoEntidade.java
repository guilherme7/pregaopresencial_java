package br.com.ghnetsoft.pregao_presencial.core.compra.servico;

import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.compra.servico.servico.Servico;
import br.com.ghnetsoft.pregao_presencial.core.entidade.Entidade;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_SERVICO_EMPRESA", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "FK_SERVICO", "FK_ENTIDADE" }, name = "U1_TAB_SERVICO_EMPRESA") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_SERVICO_EMPRESA"),
				@Index(columnList = "FK_SERVICO", name = "I2_TAB_SERVICO_EMPRESA"),
				@Index(columnList = "FK_ENTIDADE", name = "I3_TAB_SERVICO_EMPRESA") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class ServicoEntidade extends Principal {

	private static final long serialVersionUID = 5190978465572096442L;

	@Id
	@Column(name = "PK_SERVICO_EMPRESA")
	@SequenceGenerator(name = "SQ_SERVICO_EMPRESA", sequenceName = "SQ_SERVICO_EMPRESA", allocationSize = 1)
	@GeneratedValue(generator = "SQ_SERVICO_EMPRESA", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Entidade.class)
	@JoinColumn(name = "FK_ENTIDADE", nullable = false, foreignKey = @ForeignKey(name = "FK_ENTIDADE"))
	private Entidade entidade;
	@ManyToOne(targetEntity = Servico.class)
	@JoinColumn(name = "FK_SERVICO", nullable = false, foreignKey = @ForeignKey(name = "FK_SERVICO"))
	private Servico servico;
}
