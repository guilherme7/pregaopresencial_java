package br.com.ghnetsoft.pregao_presencial.core.pregao.preenchimento.impl;

import static org.apache.commons.lang3.StringUtils.leftPad;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

import br.com.ghnetsoft.pregao_presencial.core.pregao.preenchimento.PregaoPreenchimento;

/**
 * @author Guilherme C Lopes
 * 
 * @version Interface que preenche o valor de uma licitacao
 */
@Component
public class PregaoPreenchimentoImpl implements PregaoPreenchimento {

	/**
	 * @version Metodo que preenche uma licitacao
	 * 
	 * @param numero
	 * @param ano
	 * @return
	 */
	@Override
	public String preenchimento(Integer numero, Integer ano) {
		return numeroAno(numero, ano);
	}

	/**
	 * @version Metodo que preenche uma licitacao com o valor estimado
	 * 
	 * @param numero
	 * @param ano
	 * @param valorEstimado
	 * @return
	 */
	@Override
	public String preenchimentoComValor(Integer numero, Integer ano, BigDecimal valorEstimado) {
		return numeroAno(numero, ano) + " - Valor estimado: R$ " + valorEstimado;
	}

	/**
	 * Metodo que preenche uma licitação
	 * 
	 * @param numero
	 * @param ano
	 * @return
	 */
	private String numeroAno(Integer numero, Integer ano) {
		return "PP: " + leftPad(numero.toString(), 4, "0") + " - " + ano.toString();
	}
}
