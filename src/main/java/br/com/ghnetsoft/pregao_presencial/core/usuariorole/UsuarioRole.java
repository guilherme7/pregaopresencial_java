package br.com.ghnetsoft.pregao_presencial.core.usuariorole;

import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.role.Role;
import br.com.ghnetsoft.pregao_presencial.core.usuario.Usuario;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_USUARIO_ROLE", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "FK_ROLE", "FK_USUARIO" }, name = "U1_TAB_USUARIO_ROLE") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_USUARIO_ROLE"),
				@Index(columnList = "FK_USUARIO", name = "I2_TAB_USUARIO_ROLE"),
				@Index(columnList = "FK_ROLE", name = "I3_TAB_USUARIO_ROLE") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class UsuarioRole extends Principal {

	private static final long serialVersionUID = -4684869200797179364L;

	@Id
	@Column(name = "PK_USUARIO_ROLE")
	@SequenceGenerator(name = "SQ_USUARIO_ROLE", sequenceName = "SQ_USUARIO_ROLE", allocationSize = 1)
	@GeneratedValue(generator = "SQ_USUARIO_ROLE", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Role.class)
	@JoinColumn(name = "FK_ROLE", nullable = false, foreignKey = @ForeignKey(name = "FK_ROLE"))
	private Role role;
	@ManyToOne(targetEntity = Usuario.class)
	@JoinColumn(name = "FK_USUARIO", nullable = false, foreignKey = @ForeignKey(name = "FK_USUARIO"))
	private Usuario usuario;
}
