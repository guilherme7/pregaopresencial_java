package br.com.ghnetsoft.pregao_presencial.core.licitacaomodalidade;

import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_LICITACAO_MODALIDADE", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "DS_DESCRICAO" }, name = "U1_LICITACAO_MODALIDADE") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_LICITACAO_MODALIDADE"),
				@Index(columnList = "DS_DESCRICAO", name = "I2_LICITACAO_MODALIDADE"), })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class LicitacaoModalidade extends Principal {

	private static final long serialVersionUID = -3252041679074638596L;

	@Id
	@Column(name = "PK_LICITACAO_MODALIDADE")
	@SequenceGenerator(name = "SQ_LICITACAO_MODALIDADE", sequenceName = "SQ_LICITACAO_MODALIDADE", allocationSize = 1)
	@GeneratedValue(generator = "SQ_LICITACAO_MODALIDADE", strategy = SEQUENCE)
	private Long id;
	@Column(name = "DS_DESCRICAO", length = 30, nullable = false)
	private String descricao;
	@Column(name = "NR_PRAZO_RECURSO", nullable = false)
	private Integer prazoRecurso;
}
