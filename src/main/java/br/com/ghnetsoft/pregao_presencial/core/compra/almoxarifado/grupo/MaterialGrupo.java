package br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.grupo;

import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_MATERIAL_GRUPO", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "DS_CODIGO" }, name = "U1_TAB_MATERIAL_GRUPO"),
		@UniqueConstraint(columnNames = { "DS_CODIGO", "DS_DESCRICAO" }, name = "U2_TAB_MATERIAL_GRUPO") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_MATERIAL_GRUPO"),
				@Index(columnList = "DS_CODIGO", name = "I2_TAB_MATERIAL_GRUPO") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class MaterialGrupo extends Principal {

	private static final long serialVersionUID = -1488936197842307853L;

	@Id
	@Column(name = "PK_MATERIAL_GRUPO")
	@SequenceGenerator(name = "SQ_MATERIAL_GRUPO", sequenceName = "SQ_MATERIAL_GRUPO", allocationSize = 1)
	@GeneratedValue(generator = "SQ_MATERIAL_GRUPO", strategy = SEQUENCE)
	private Long id;
	@Column(name = "DS_CODIGO", nullable = false, length = 2)
	private String codigo;
	@Column(name = "DS_DESCRICAO", nullable = false)
	private String descricao;
}
