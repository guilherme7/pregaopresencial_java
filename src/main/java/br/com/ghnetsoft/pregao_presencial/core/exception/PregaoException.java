package br.com.ghnetsoft.pregao_presencial.core.exception;

import java.util.Collection;

import br.com.ghnetsoft.principal.utilitario.mensagem.Mensagem;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Excecao para o pregao
 */
@Getter
@Setter
public class PregaoException extends RuntimeException {

	private static final long serialVersionUID = -4563523333783230742L;

	private Collection<Mensagem> mensagens;

	/**
	 * @version Insere os valores na excecao
	 * 
	 * @param mensagens
	 */
	public PregaoException(Collection<Mensagem> mensagens) {
		super();
		this.mensagens = mensagens;
	}
}
