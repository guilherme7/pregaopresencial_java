package br.com.ghnetsoft.pregao_presencial.core.negociacao;

import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.pessoa.Pessoa;
import br.com.ghnetsoft.pregao_presencial.core.pregao.Pregao;
import br.com.ghnetsoft.pregao_presencial.core.pregaoitem.PregaoItem;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_NEGOCIACAO", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "FK_PESSOA", "FK_PREGAO", "VR_ATUAL",
				"VR_NEGOCIACAO" }, name = "U1_TAB_NEGOCIACAO"),
		@UniqueConstraint(columnNames = { "FK_PESSOA", "FK_PREGAO", "FK_PREGAO_ITEM", "VR_ATUAL",
				"VR_NEGOCIACAO" }, name = "U2_TAB_NEGOCIACAO") }, indexes = {
						@Index(columnList = "ST_REGISTRO", name = "I1_TAB_NEGOCIACAO"),
						@Index(columnList = "FK_PREGAO", name = "I2_TAB_NEGOCIACAO"),
						@Index(columnList = "FK_PREGAO_ITEM", name = "I3_TAB_NEGOCIACAO"),
						@Index(columnList = "FK_PESSOA", name = "I4_TAB_NEGOCIACAO") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class Negociacao extends Principal {

	private static final long serialVersionUID = 1121328259788024844L;

	@Id
	@Column(name = "PK_NEGOCIACAO")
	@SequenceGenerator(name = "SQ_NEGOCIACAO", sequenceName = "SQ_NEGOCIACAO", allocationSize = 1)
	@GeneratedValue(generator = "SQ_NEGOCIACAO", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Pregao.class)
	@JoinColumn(name = "FK_PREGAO", nullable = false, foreignKey = @ForeignKey(name = "FK_PREGAO"))
	private Pregao pregao;
	@ManyToOne(targetEntity = PregaoItem.class)
	@JoinColumn(name = "FK_PREGAO_ITEM", foreignKey = @ForeignKey(name = "FK_ITEM"))
	private PregaoItem item;
	@ManyToOne(targetEntity = Pessoa.class)
	@JoinColumn(name = "FK_PESSOA", nullable = false, foreignKey = @ForeignKey(name = "FK_PESSOA"))
	private Pessoa pessoa;
	@Column(name = "VR_ATUAL", nullable = false, length = 25, precision = 2)
	private BigDecimal atual;
	@Column(name = "DS_OBSERVACAO", nullable = false, length = 4000, precision = 2)
	private String observacao;
	@Column(name = "VR_NEGOCIACAO", nullable = false, length = 25, precision = 2)
	private BigDecimal negociacao;
}
