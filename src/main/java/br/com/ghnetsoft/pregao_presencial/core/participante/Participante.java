package br.com.ghnetsoft.pregao_presencial.core.participante;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.pessoa.Pessoa;
import br.com.ghnetsoft.pregao_presencial.core.pregao.Pregao;
import br.com.ghnetsoft.pregao_presencial.core.pregaoitem.PregaoItem;
import br.com.ghnetsoft.principal.core.enuns.SimNaoEnum;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_PARTICIPANTE", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "FK_PESSOA", "FK_PREGAO_ITEM" }, name = "U1_TAB_PARTICIPANTE"),
		@UniqueConstraint(columnNames = { "DS_CPF", "FK_PREGAO_ITEM" }, name = "U2_TAB_PARTICIPANTE"),
		@UniqueConstraint(columnNames = { "FK_PESSOA", "FK_PREGAO" }, name = "U3_TAB_PARTICIPANTE"),
		@UniqueConstraint(columnNames = { "DS_CPF", "FK_PREGAO" }, name = "U4_TAB_PARTICIPANTE") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_PARTICIPANTE"),
				@Index(columnList = "FK_PESSOA", name = "I2_TAB_PARTICIPANTE"),
				@Index(columnList = "FK_PREGAO_ITEM", name = "I3_TAB_PARTICIPANTE"),
				@Index(columnList = "DS_CPF", name = "I4_TAB_PARTICIPANTE"),
				@Index(columnList = "FK_PREGAO", name = "I5_TAB_PARTICIPANTE") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class Participante extends Principal {

	private static final long serialVersionUID = -7394262138474366448L;

	@Id
	@Column(name = "PK_PARTICIPANTE")
	@SequenceGenerator(name = "SQ_PARTICIPANTE", sequenceName = "SQ_PARTICIPANTE", allocationSize = 1)
	@GeneratedValue(generator = "SQ_PARTICIPANTE", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Pregao.class)
	@JoinColumn(name = "FK_PREGAO", nullable = false, foreignKey = @ForeignKey(name = "FK_PREGAO"))
	private Pregao pregao;
	@ManyToOne(targetEntity = PregaoItem.class)
	@JoinColumn(name = "FK_PREGAO_ITEM", foreignKey = @ForeignKey(name = "FK_ITEM"))
	private PregaoItem item;
	@ManyToOne(targetEntity = Pessoa.class)
	@JoinColumn(name = "FK_PESSOA", foreignKey = @ForeignKey(name = "FK_PESSOA"))
	private Pessoa fornecedor;
	@Enumerated(STRING)
	@Column(name = "ST_HABILITADO", nullable = false)
	private SimNaoEnum habilitado;
	@Column(name = "DS_REPRESENTANTE", length = 100)
	private String representante;
	@Column(name = "DS_CPF", length = 11)
	private String cpf;
	@Column(name = "DS_IDENTIDADE", length = 20)
	private String identidade;
	@Column(name = "VR_VALOR", length = 25, precision = 2)
	private BigDecimal valor;
}
