package br.com.ghnetsoft.pregao_presencial.core.usuario.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ghnetsoft.pregao_presencial.core.usuario.Usuario;
import br.com.ghnetsoft.pregao_presencial.core.usuario.UsuarioConsultaService;

/**
 * Classe que envia as informacoes de usuario para consultar no banco de dados
 * 
 * @author Guilherme C Lopes
 */
@Service
public class UsuarioConsultaServiceImpl implements UsuarioConsultaService {

	@Autowired
	private UsuarioRepository repository;

	/**
	 * Metodo para retornar o usuario por login
	 * 
	 * @param login
	 * @return
	 */
	@Override
	public Optional<Usuario> buscarPeloLogin(String login) {
		return repository.findByLogin(login);
	}

	/**
	 * Metodo para retornar o usuario por email
	 * 
	 * @param login
	 * @return
	 */
	@Override
	public Optional<Usuario> buscarPeloEmail(String email) {
		return repository.findByEmail(email);
	}
}
