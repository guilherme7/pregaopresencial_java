package br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.materialitem.preenchimento;

import org.springframework.stereotype.Component;

import br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.materialitem.MaterialItemPreenchimento;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe que preenche o valor de um item do material
 */
@Component
public class MaterialItemPreenchimentoImpl implements MaterialItemPreenchimento {

	/**
	 * @version Metodo que preenche um item do material
	 * 
	 * @param federalSulpply
	 * @param descricao
	 * @param unidade
	 * @return
	 */
	@Override
	public String federalSulpplyNomeUnidade(String federalSulpply, String descricao, String unidade) {
		return "Und: " + unidade + " - FederalSulpply: " + federalSulpply + " - " + descricao;
	}
}
