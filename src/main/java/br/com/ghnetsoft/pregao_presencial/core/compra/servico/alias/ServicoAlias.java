package br.com.ghnetsoft.pregao_presencial.core.compra.servico.alias;

import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_SERVICO_ALIAS", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "DS_DESCRICAO" }, name = "U1_TAB_SERVICO_ALIAS") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_SERVICO_ALIAS"),
				@Index(columnList = "DS_DESCRICAO", name = "I2_TAB_SERVICO_ALIAS") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class ServicoAlias extends Principal {

	private static final long serialVersionUID = 3685189356312929263L;

	@Id
	@Column(name = "PK_SERVICO_ALIAS")
	@SequenceGenerator(name = "SQ_SERVICO_ALIAS", sequenceName = "SQ_SERVICO_ALIAS", allocationSize = 1)
	@GeneratedValue(generator = "SQ_SERVICO_ALIAS", strategy = SEQUENCE)
	private Long id;
	@Column(name = "DS_DESCRICAO", nullable = false)
	private String descricao;
}
