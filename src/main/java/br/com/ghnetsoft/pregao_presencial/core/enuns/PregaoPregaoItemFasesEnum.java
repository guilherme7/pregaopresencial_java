package br.com.ghnetsoft.pregao_presencial.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Enum da fase do pregao e do item
 */
@Getter
public enum PregaoPregaoItemFasesEnum {

	NAO_INICIOU("Não iniciou"), INICIOU("Iniciou"), CLAS_INICIL("Classificação inicial"), LANCES("Lançes"),
	DESISTENCIA("Desistência"), CLAS_FORNEC("Classificação dos fornecedores"), ME_EPP("ME/EPP"),
	NEGOCIACAO("Negociação"), FINALIZADO("Finalizado"), CANCELADO("Cancelado"), ANULADO("Anulado");

	private String descricao;

	/**
	 * @version construtor
	 * 
	 * @param descricao
	 */
	PregaoPregaoItemFasesEnum(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @version Busca a fase do pregao ou do item pelo valor
	 * 
	 * @param descricao
	 * @return
	 */
	public static PregaoPregaoItemFasesEnum buscaPregaoPregaoItemFasesEnum(String descricao) {
		for (PregaoPregaoItemFasesEnum enun : PregaoPregaoItemFasesEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}
}
