package br.com.ghnetsoft.pregao_presencial.core.exception;

import java.util.Collection;

import br.com.ghnetsoft.principal.utilitario.mensagem.Mensagem;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Excecao para competencia
 */
@Getter
@Setter
public class CompetenciaException extends RuntimeException {

	private static final long serialVersionUID = -3843937867732594909L;

	private Collection<Mensagem> mensagens;

	/**
	 * @version Insere os valores na excecao
	 * 
	 * @param mensagens
	 */
	public CompetenciaException(Collection<Mensagem> mensagens) {
		super();
		this.mensagens = mensagens;
	}
}
