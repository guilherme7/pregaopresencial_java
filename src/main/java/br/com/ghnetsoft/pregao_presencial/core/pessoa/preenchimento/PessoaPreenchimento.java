package br.com.ghnetsoft.pregao_presencial.core.pessoa.preenchimento;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe que preenche o valor de uma pessoa
 */
public interface PessoaPreenchimento {

	/**
	 * @version Metodo que preenche uma pessoa
	 * 
	 * @param nomeRazaoSocial
	 * @param documento
	 * @return
	 */
	String preenchimento(String nomeRazaoSocial, String documento);
}
