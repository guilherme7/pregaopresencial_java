package br.com.ghnetsoft.pregao_presencial.core.competencia.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ghnetsoft.pregao_presencial.core.competencia.Competencia;
import br.com.ghnetsoft.pregao_presencial.core.competencia.CompetenciaConsultaService;
import br.com.ghnetsoft.pregao_presencial.core.competencia.preenchimento.CompetenciaPreenchimento;
import br.com.ghnetsoft.pregao_presencial.core.competencia.resource.CompetenciaSimplesRetornoResource;

@Service
public class CompetenciaConsultaServiceImpl implements CompetenciaConsultaService {

	@Autowired
	private CompetenciaRepository repository;
	@Autowired
	private CompetenciaPreenchimento competenciaPreenchimento;

	@Override
	public Collection<CompetenciaSimplesRetornoResource> ativosInativosPorEntidade(Long idEntidade) {
		Collection<Competencia> competencias = repository.ativosInativosPorEntidade(idEntidade);
		Collection<CompetenciaSimplesRetornoResource> retorno = new ArrayList<>();
		competencias.forEach(competencia -> {
			CompetenciaSimplesRetornoResource resource = CompetenciaSimplesRetornoResource.builder().mesAno(
					competenciaPreenchimento.preenchimento(competencia.getMes().getDescricao(), competencia.getAno()))
					.build();
			resource.setId(competencia.getId());
			retorno.add(resource);
		});
		return retorno;
	}
}
