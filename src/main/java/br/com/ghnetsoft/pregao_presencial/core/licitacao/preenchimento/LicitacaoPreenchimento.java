package br.com.ghnetsoft.pregao_presencial.core.licitacao.preenchimento;

/**
 * @author Guilherme C Lopes
 * 
 * @version Interface que preenche o valor de uma licitacao
 */

public interface LicitacaoPreenchimento {

	/**
	 * @version Metodo que preenche uma licitacao
	 * 
	 * @param numero
	 * @param ano
	 * @return
	 */
	String preenchimento(Integer numero, Integer ano);
}
