package br.com.ghnetsoft.pregao_presencial.core.fase;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.ghnetsoft.pregao_presencial.core.enuns.PregaoPregaoItemFasesEnum;
import br.com.ghnetsoft.pregao_presencial.core.pregao.Pregao;
import br.com.ghnetsoft.pregao_presencial.core.pregaoitem.PregaoItem;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_FASE", indexes = { @Index(columnList = "ST_REGISTRO", name = "I1_TAB_FASE"),
		@Index(columnList = "FK_PREGAO", name = "I2_TAB_FASE"),
		@Index(columnList = "FK_PREGAO_ITEM", name = "I3_TAB_FASE"),
		@Index(columnList = "ST_FASE", name = "I4_TAB_FASE") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class Fase extends Principal {

	private static final long serialVersionUID = 6919600978506706903L;

	@Id
	@Column(name = "PK_FASE")
	@SequenceGenerator(name = "SQ_FASE", sequenceName = "SQ_FASE", allocationSize = 1)
	@GeneratedValue(generator = "SQ_FASE", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Pregao.class)
	@JoinColumn(name = "FK_PREGAO", nullable = false, foreignKey = @ForeignKey(name = "FK_PREGAO"))
	private Pregao pregao;
	@ManyToOne(targetEntity = PregaoItem.class)
	@JoinColumn(name = "FK_PREGAO_ITEM", foreignKey = @ForeignKey(name = "FK_ITEM"))
	private PregaoItem item;
	@Enumerated(STRING)
	@Column(name = "ST_FASE", nullable = false, length = 20)
	private PregaoPregaoItemFasesEnum fase;
}
