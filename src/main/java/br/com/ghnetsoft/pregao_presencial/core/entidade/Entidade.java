package br.com.ghnetsoft.pregao_presencial.core.entidade;

import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_ENTIDADE", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "DS_CNPJ" }, name = "U1_TAB_ENTIDADE") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_ENTIDADE"),
				@Index(columnList = "DS_CNPJ", name = "I2_TAB_ENTIDADE") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class Entidade extends Principal {

	private static final long serialVersionUID = -4906221230296637058L;

	@Id
	@Column(name = "PK_EMPRESA")
	@SequenceGenerator(name = "SQ_EMPRESA", sequenceName = "SQ_EMPRESA", allocationSize = 1)
	@GeneratedValue(generator = "SQ_EMPRESA", strategy = SEQUENCE)
	private Long id;
	@Column(name = "DS_RAZAO_SOCIAL", nullable = false)
	private String razaoSocial;
	@Column(name = "DS_CNPJ", nullable = false, length = 14)
	private String cnpj;
}
