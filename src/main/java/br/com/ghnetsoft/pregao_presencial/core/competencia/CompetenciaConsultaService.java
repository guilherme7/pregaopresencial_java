package br.com.ghnetsoft.pregao_presencial.core.competencia;

import java.util.Collection;

import br.com.ghnetsoft.pregao_presencial.core.competencia.resource.CompetenciaSimplesRetornoResource;

public interface CompetenciaConsultaService {

	Collection<CompetenciaSimplesRetornoResource> ativosInativosPorEntidade(Long idEntidade);
}
