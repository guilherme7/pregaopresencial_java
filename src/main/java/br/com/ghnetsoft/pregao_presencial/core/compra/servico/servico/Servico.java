package br.com.ghnetsoft.pregao_presencial.core.compra.servico.servico;

import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.compra.servico.alias.ServicoAlias;
import br.com.ghnetsoft.pregao_presencial.core.compra.servico.cpc.ServicoCpc;
import br.com.ghnetsoft.pregao_presencial.core.compra.servico.grupo.ServicoGrupo;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_SERVICO", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "DS_CODIGO" }, name = "U1_TAB_SERVICO") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_SERVICO"),
				@Index(columnList = "DS_CODIGO", name = "I3_TAB_SERVICO"),
				@Index(columnList = "FK_SERVICO_GRUPO", name = "I3_TAB_SERVICO"),
				@Index(columnList = "FK_SERVICO_CPC", name = "I4_TAB_SERVICO") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class Servico extends Principal {

	private static final long serialVersionUID = -7731792538078973614L;

	@Id
	@Column(name = "PK_SERVICO")
	@SequenceGenerator(name = "SQ_SERVICO", sequenceName = "SQ_SERVICO", allocationSize = 1)
	@GeneratedValue(generator = "SQ_SERVICO", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = ServicoGrupo.class)
	@JoinColumn(name = "FK_SERVICO_GRUPO", nullable = false, foreignKey = @ForeignKey(name = "FK_SERVICO_GRUPO"))
	private ServicoGrupo grupo;
	@ManyToOne(targetEntity = ServicoCpc.class)
	@JoinColumn(name = "FK_SERVICO_CPC", foreignKey = @ForeignKey(name = "FK_SERVICO_CPC"))
	private ServicoCpc cpc;
	@ManyToOne(targetEntity = ServicoAlias.class)
	@JoinColumn(name = "FK_SERVICO_ALIAS_UM", foreignKey = @ForeignKey(name = "FK_SERVICO_ALIAS_UM"))
	private ServicoAlias aliasUm;
	@ManyToOne(targetEntity = ServicoAlias.class)
	@JoinColumn(name = "FK_SERVICO_ALIAS_DOIS", foreignKey = @ForeignKey(name = "FK_SERVICO_ALIAS_DOIS"))
	private ServicoAlias aliasDois;
	@ManyToOne(targetEntity = ServicoAlias.class)
	@JoinColumn(name = "FK_SERVICO_ALIAS_TRES", foreignKey = @ForeignKey(name = "FK_SERVICO_ALIAS_TRES"))
	private ServicoAlias aliasTres;
	@ManyToOne(targetEntity = ServicoAlias.class)
	@JoinColumn(name = "FK_SERVICO_ALIAS_QUATRO", foreignKey = @ForeignKey(name = "FK_SERVICO_ALIAS_QUATRO"))
	private ServicoAlias aliasQuatro;
	@ManyToOne(targetEntity = ServicoAlias.class)
	@JoinColumn(name = "FK_SERVICO_ALIAS_CINCO", foreignKey = @ForeignKey(name = "FK_SERVICO_ALIAS_CINCO"))
	private ServicoAlias aliasCinco;
	@Column(name = "DS_CODIGO", nullable = false, length = 9)
	private String codigo;
	@Column(name = "DS_DESCRICAO", nullable = false, length = 4000)
	private String descricao;
}
