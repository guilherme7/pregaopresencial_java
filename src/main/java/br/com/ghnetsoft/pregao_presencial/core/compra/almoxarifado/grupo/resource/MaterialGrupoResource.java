package br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.grupo.resource;

import static lombok.AccessLevel.PROTECTED;

import br.com.ghnetsoft.principal.core.resorce.PrincipalRetornoIdResource;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Resoure que retorna codigo e descricao do material grupo
 */
@Setter
@Getter
@Builder
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class MaterialGrupoResource extends PrincipalRetornoIdResource {

	private static final long serialVersionUID = 1869010326158511436L;
	private String codigo;
	private String descricao;
}
