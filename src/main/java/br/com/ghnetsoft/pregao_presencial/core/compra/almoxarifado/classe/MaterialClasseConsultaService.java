package br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.classe;

import java.util.Collection;

import br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.classe.resource.MaterialClasseResource;

/**
 * @author Guilherme C Lopes
 * 
 * @version Interface que envia as informações de material classe para consultar
 *          no banco de dados
 */
public interface MaterialClasseConsultaService {

	Collection<MaterialClasseResource> buscarTodos();
}
