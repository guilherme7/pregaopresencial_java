package br.com.ghnetsoft.pregao_presencial.core.pregaoitem;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.SEQUENCE;
import static javax.persistence.TemporalType.TIMESTAMP;
import static lombok.AccessLevel.PROTECTED;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.competencia.Competencia;
import br.com.ghnetsoft.pregao_presencial.core.enuns.PregaoPregaoItemFasesEnum;
import br.com.ghnetsoft.pregao_presencial.core.licitacaoitem.LicitacaoItem;
import br.com.ghnetsoft.pregao_presencial.core.participante.Participante;
import br.com.ghnetsoft.pregao_presencial.core.pregao.Pregao;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_PREGAO_ITEM", uniqueConstraints = { @UniqueConstraint(columnNames = { "FK_PREGAO",
		"FK_LICITACAO_ITEM", "FK_COMPETENCIA" }, name = "U1_TAB_PREGAO_ITEM") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_PREGAO_ITEM"),
				@Index(columnList = "FK_PREGAO", name = "I2_TAB_PREGAO_ITEM"),
				@Index(columnList = "FK_LICITACAO_ITEM", name = "I3_TAB_PREGAO_ITEM"),
				@Index(columnList = "FK_COMPETENCIA", name = "I4_TAB_PREGAO_ITEM") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class PregaoItem extends Principal {

	private static final long serialVersionUID = 4799837330660433647L;

	@Id
	@Column(name = "PK_PREGAO_ITEM")
	@SequenceGenerator(name = "SQ_PREGAO_ITEM", sequenceName = "SQ_PREGAO_ITEM", allocationSize = 1)
	@GeneratedValue(generator = "SQ_PREGAO_ITEM", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Pregao.class)
	@JoinColumn(name = "FK_PREGAO", nullable = false, foreignKey = @ForeignKey(name = "FK_PREGAO"))
	private Pregao pregao;
	@ManyToOne(targetEntity = LicitacaoItem.class)
	@JoinColumn(name = "FK_LICITACAO_ITEM", nullable = false, foreignKey = @ForeignKey(name = "FK_LICITACAO_ITEM"))
	private LicitacaoItem licitacaoItem;
	@ManyToOne(targetEntity = Competencia.class)
	@JoinColumn(name = "FK_COMPETENCIA", nullable = false, foreignKey = @ForeignKey(name = "FK_COMPETENCIA"))
	private Competencia competencia;
	@Enumerated(STRING)
	@Column(name = "ST_FASE", nullable = false, length = 20)
	private PregaoPregaoItemFasesEnum fase;
	@Column(name = "VR_MINIMO_POR_LANCE", length = 25, precision = 2)
	private BigDecimal valorMinimoPorLance;
	@Column(name = "NR_QUANTIDADE_FORNECEDOR")
	private Integer quantidadeFornecedor;
	@Temporal(TIMESTAMP)
	@Column(name = "TS_DATA_HORA_ABERTURA")
	private Date dataHoraAbertura;
	@Temporal(TIMESTAMP)
	@Column(name = "TS_DATA_HORA_FECHOU")
	private Date dataHoraFechou;
	@OneToMany(mappedBy = "item", targetEntity = Participante.class)
	private Collection<Participante> participantes;
}
