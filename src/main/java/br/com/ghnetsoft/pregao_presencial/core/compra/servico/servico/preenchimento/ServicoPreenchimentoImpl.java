package br.com.ghnetsoft.pregao_presencial.core.compra.servico.servico.preenchimento;

import org.springframework.stereotype.Component;

import br.com.ghnetsoft.pregao_presencial.core.compra.servico.servico.ServicoPreenchimento;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe que preenche o valor de um servico
 */
@Component
public class ServicoPreenchimentoImpl implements ServicoPreenchimento {

	/**
	 * @version Metodo que preenche um servico
	 * 
	 * @param grupo
	 * @param codigo
	 * @param descricao
	 * @return
	 */
	@Override
	public String grupoCodigoDescricao(String grupo, String codigo, String descricao) {
		return "Grupo: " + grupo + " - Código: " + codigo + " - " + descricao;
	}
}
