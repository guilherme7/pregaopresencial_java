package br.com.ghnetsoft.pregao_presencial.core.pregao;

import java.util.Collection;

import br.com.ghnetsoft.pregao_presencial.core.pregao.resource.PregaoPesquisaEnvioResource;
import br.com.ghnetsoft.pregao_presencial.core.pregao.resource.PregaoPesquisaPaginacaoResource;
import br.com.ghnetsoft.pregao_presencial.core.pregao.resource.PregaoSimplesRetornoResource;

public interface PregaoConsultaService {

	Collection<PregaoSimplesRetornoResource> ativosPorUsuario(Long idUsuario);

	PregaoPesquisaPaginacaoResource paginacaoPregao(PregaoPesquisaEnvioResource resource);
}
