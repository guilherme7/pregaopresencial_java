package br.com.ghnetsoft.pregao_presencial.core.competencia.impl;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.ghnetsoft.pregao_presencial.core.competencia.Competencia;

@Repository
interface CompetenciaRepository extends JpaRepository<Competencia, Long> {

	@Query(value = "SELECT e FROM Competencia e WHERE e.entidade.id = :idEntidade "
			+ "AND e.statusDoRegistro IN ('ATIVO', 'INATIVO') ORDER BY e.ano DESC, e.mes ASC ")
	Collection<Competencia> ativosInativosPorEntidade(Long idEntidade);
}
