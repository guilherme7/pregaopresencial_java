package br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.grupo.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.grupo.MaterialGrupo;
import br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.grupo.MaterialGrupoConsultaService;
import br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.grupo.resource.MaterialGrupoResource;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe que envia as informações de material grupo para consultar no
 *          banco de dados
 */
@Service
public class MaterialGrupoConsultaServiceImpl implements MaterialGrupoConsultaService {

	@Autowired
	private MaterialGrupoRepository repository;

	/**
	 * @version Metodo que busca todos materiais
	 * 
	 * @param idEntidade
	 * @return
	 */
	@Override
	public Collection<MaterialGrupoResource> porEntidade(Long idEntidade) {
		Collection<MaterialGrupo> materiaisGrupos = repository.findAll();
		Collection<MaterialGrupoResource> retorno = new ArrayList<>();
		materiaisGrupos.forEach(materialGrupo -> {
			MaterialGrupoResource resource = MaterialGrupoResource.builder().codigo(materialGrupo.getCodigo())
					.descricao(materialGrupo.getDescricao()).build();
			resource.setId(materialGrupo.getId());
			retorno.add(resource);
		});
		return retorno;
	}
}
