package br.com.ghnetsoft.pregao_presencial.core.pregao.resource;

import static lombok.AccessLevel.PROTECTED;

import br.com.ghnetsoft.principal.core.resorce.PaginacaoEnvioResource;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class PregaoPesquisaEnvioResource extends PaginacaoEnvioResource {

	private static final long serialVersionUID = -6820064186157253945L;

	// pregao
	private Long idEntidade;
	private Long idPregao;
	private Long idCompetenciaPregao;
	private String fasePregao;
	// licitacao
	private String regime;
	private Long idCompetenciaLicitacao;
	private String valorEstimado;
}
