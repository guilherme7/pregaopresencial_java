package br.com.ghnetsoft.pregao_presencial.core.competencia.preenchimento;

public interface CompetenciaPreenchimento {

	String preenchimento(String mes, Integer ano);
}
