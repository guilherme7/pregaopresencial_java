package br.com.ghnetsoft.pregao_presencial.core.role;

import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_ROLE", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "DS_NOME", "DS_ROLE" }, name = "U1_TAB_ROLE") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_ROLE"),
				@Index(columnList = "DS_NOME", name = "I2_TAB_ROLE"),
				@Index(columnList = "DS_ROLE", name = "I2_TAB_ROLE") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class Role extends Principal { // implements GrantedAuthority {

	private static final long serialVersionUID = 7203996303885284451L;

	@Id
	@Column(name = "PK_ROLE")
	@SequenceGenerator(name = "SQ_ROLE", sequenceName = "SQ_ROLE", allocationSize = 1)
	@GeneratedValue(generator = "SQ_ROLE", strategy = SEQUENCE)
	private Long id;
	@Column(name = "DS_NOME", length = 50, nullable = false)
	private String nome;
	@Column(name = "DS_ROLE", length = 50, nullable = false)
	private String role;

	/*
	 * @Override public String getAuthority() { return role; }
	 */
}
