package br.com.ghnetsoft.pregao_presencial.core.enuns.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Component;

import br.com.ghnetsoft.pregao_presencial.core.enuns.EnunsService;
import br.com.ghnetsoft.pregao_presencial.core.enuns.LicitacaoRegimeEnum;
import br.com.ghnetsoft.pregao_presencial.core.enuns.PregaoPregaoItemFasesEnum;
import br.com.ghnetsoft.principal.core.resorce.EnumResource;

@Component
public class EnunsServiceImpl implements EnunsService {

	@Override
	public Collection<EnumResource> regimes() {
		Collection<EnumResource> retorno = new ArrayList<>();
		for (LicitacaoRegimeEnum enun : LicitacaoRegimeEnum.values()) {
			retorno.add(EnumResource.builder().key(enun.name()).texto(enun.getDescricao()).build());
		}
		return retorno;
	}

	@Override
	public Collection<EnumResource> Fases() {
		Collection<EnumResource> retorno = new ArrayList<>();
		for (PregaoPregaoItemFasesEnum enun : PregaoPregaoItemFasesEnum.values()) {
			retorno.add(EnumResource.builder().key(enun.name()).texto(enun.getDescricao()).build());
		}
		return retorno;
	}
}
