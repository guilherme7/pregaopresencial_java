package br.com.ghnetsoft.pregao_presencial.core.entidade.resource;

import static lombok.AccessLevel.PROTECTED;

import br.com.ghnetsoft.principal.core.resorce.PrincipalRetornoIdResource;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class EntidadeSimplesResource extends PrincipalRetornoIdResource {

	private static final long serialVersionUID = -762691218952825998L;

	private String cnpjNome;
}
