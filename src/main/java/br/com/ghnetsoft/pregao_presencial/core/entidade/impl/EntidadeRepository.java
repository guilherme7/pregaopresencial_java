package br.com.ghnetsoft.pregao_presencial.core.entidade.impl;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.ghnetsoft.pregao_presencial.core.entidade.Entidade;

@Repository
interface EntidadeRepository extends JpaRepository<Entidade, Long> {

	@Query(value = "SELECT e FROM Entidade e WHERE e IN "
			+ "(SELECT eu.entidade FROM EntidadeUsuario eu WHERE eu.usuario.id = :idUsuario) "
			+ "AND e.statusDoRegistro = 'ATIVO' ORDER BY e.cnpj ASC ")
	Collection<Entidade> todosAtivosPorUsuario(Long idUsuario);
}
