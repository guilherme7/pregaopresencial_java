package br.com.ghnetsoft.pregao_presencial.core.entidade.preenchimento;

public interface EntidadePreenchimento {

	String preenchimento(String cnpj, String razaoSocial);
}
