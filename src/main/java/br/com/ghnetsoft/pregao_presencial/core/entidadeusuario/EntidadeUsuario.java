package br.com.ghnetsoft.pregao_presencial.core.entidadeusuario;

import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.entidade.Entidade;
import br.com.ghnetsoft.pregao_presencial.core.usuario.Usuario;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_ENTIDADE_USUARIO", uniqueConstraints = { @UniqueConstraint(columnNames = { "FK_USUARIO",
		"FK_ENTIDADE" }, name = "U1_TAB_ENTIDADE_USUARIO") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_ENTIDADE_USUARIO"),
				@Index(columnList = "FK_USUARIO", name = "I2_TAB_ENTIDADE_USUARIO"),
				@Index(columnList = "FK_ENTIDADE", name = "I2_TAB_ENTIDADE_USUARIO") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class EntidadeUsuario extends Principal {

	private static final long serialVersionUID = -1340841683594672167L;

	@Id
	@Column(name = "PK_ENTIDADE_USUARIO")
	@SequenceGenerator(name = "SQ_ENTIDADE_USUARIO", sequenceName = "SQ_ENTIDADE_USUARIO", allocationSize = 1)
	@GeneratedValue(generator = "SQ_ENTIDADE_USUARIO", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Usuario.class)
	@JoinColumn(name = "FK_USUARIO", nullable = false, foreignKey = @ForeignKey(name = "FK_USUARIO"))
	private Usuario usuario;
	@ManyToOne(targetEntity = Entidade.class)
	@JoinColumn(name = "FK_ENTIDADE", nullable = false, foreignKey = @ForeignKey(name = "FK_ENTIDADE"))
	private Entidade entidade;
}
