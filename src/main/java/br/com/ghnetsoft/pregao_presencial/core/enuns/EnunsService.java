package br.com.ghnetsoft.pregao_presencial.core.enuns;

import java.util.Collection;

import br.com.ghnetsoft.principal.core.resorce.EnumResource;

public interface EnunsService {

	Collection<EnumResource> regimes();

	Collection<EnumResource> Fases();
}
