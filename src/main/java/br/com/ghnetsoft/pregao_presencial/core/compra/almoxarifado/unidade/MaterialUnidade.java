package br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.unidade;

import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.material.Material;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_MATERIAL_UNIDADE", uniqueConstraints = { @UniqueConstraint(columnNames = { "FK_MATERIAL",
		"DS_DESCRICAO", "NR_CAPACIDADE", "DS_UNIDADE_MEDIDA" }, name = "U1_TAB_MATERIAL_UNIDADE") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_MATERIAL_UNIDADE"),
				@Index(columnList = "FK_MATERIAL", name = "I2_TAB_MATERIAL_UNIDADE"),
				@Index(columnList = "DS_DESCRICAO", name = "I3_TAB_MATERIAL_UNIDADE"),
				@Index(columnList = "NR_CAPACIDADE", name = "I4_TAB_MATERIAL_UNIDADE"),
				@Index(columnList = "DS_UNIDADE_MEDIDA", name = "I5_TAB_MATERIAL_UNIDADE") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class MaterialUnidade extends Principal {

	private static final long serialVersionUID = 93313078897066516L;

	@Id
	@Column(name = "PK_MATERIAL_UNIDADE")
	@SequenceGenerator(name = "SQ_MATERIAL_UNIDADE", sequenceName = "SQ_MATERIAL_UNIDADE", allocationSize = 1)
	@GeneratedValue(generator = "SQ_MATERIAL_UNIDADE", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Material.class)
	@JoinColumn(name = "FK_MATERIAL", nullable = false, foreignKey = @ForeignKey(name = "FK_MATERIAL"))
	private Material material;
	@Column(name = "DS_DESCRICAO", nullable = false, length = 50)
	private String descricao;
	@Column(name = "NR_CAPACIDADE", nullable = false)
	private Integer capacidade;
	@Column(name = "DS_UNIDADE_MEDIDA", nullable = false, length = 10)
	private String unidadeMedida;
}
