package br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.grupo.impl;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.grupo.MaterialGrupo;

@Repository
interface MaterialGrupoRepository extends JpaRepository<MaterialGrupo, Long> {
}
