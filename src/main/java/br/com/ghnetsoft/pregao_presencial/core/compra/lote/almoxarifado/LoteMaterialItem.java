package br.com.ghnetsoft.pregao_presencial.core.compra.lote.almoxarifado;

import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.materialitem.MaterialItem;
import br.com.ghnetsoft.pregao_presencial.core.compra.lote.Lote;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_LOTE_MATERIAL_ITEM", uniqueConstraints = { @UniqueConstraint(columnNames = { "FK_LOTE",
		"FK_MATERIAL_ITEM", "NR_QUANTIDADE" }, name = "U1_TAB_LOTE_MATERIAL_ITEM") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_LOTE_MATERIAL_ITEM"),
				@Index(columnList = "FK_LOTE", name = "I2_TAB_LOTE_MATERIAL_ITEM"),
				@Index(columnList = "FK_MATERIAL_ITEM", name = "I3_TAB_LOTE_MATERIAL_ITEM") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class LoteMaterialItem extends Principal {

	private static final long serialVersionUID = -3721342465144323862L;

	@Id
	@Column(name = "PK_LOTE_MATERIAL_ITEM")
	@SequenceGenerator(name = "SQ_LOTE_MATERIAL_ITEM", sequenceName = "SQ_LOTE_MATERIAL_ITEM", allocationSize = 1)
	@GeneratedValue(generator = "SQ_LOTE_MATERIAL_ITEM", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Lote.class)
	@JoinColumn(name = "FK_LOTE", nullable = false, foreignKey = @ForeignKey(name = "FK_LOTE"))
	private Lote lote;
	@ManyToOne(targetEntity = MaterialItem.class)
	@JoinColumn(name = "FK_MATERIAL_ITEM", nullable = false, foreignKey = @ForeignKey(name = "FK_MATERIAL_ITEM"))
	private MaterialItem materialItem;
	@Column(name = "NR_QUANTIDADE", length = 25, precision = 2)
	private BigDecimal quantidade;
}
