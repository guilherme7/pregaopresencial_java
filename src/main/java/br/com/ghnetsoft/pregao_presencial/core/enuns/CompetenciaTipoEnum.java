package br.com.ghnetsoft.pregao_presencial.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Enum do tipo de competencia
 */
@Getter
public enum CompetenciaTipoEnum {

	ABERTURA("Abertura"), FECHAMENTO("Fechamento");

	private String descricao;

	/**
	 * @version construtor
	 * 
	 * @param descricao
	 */
	CompetenciaTipoEnum(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @version Busca o tipo de competencia pelo valor
	 * 
	 * @param descricao
	 * @return
	 */
	public static CompetenciaTipoEnum buscaTipoMesEnum(String descricao) {
		for (CompetenciaTipoEnum enun : CompetenciaTipoEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}
}
