package br.com.ghnetsoft.pregao_presencial.core.entidade.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ghnetsoft.pregao_presencial.core.entidade.Entidade;
import br.com.ghnetsoft.pregao_presencial.core.entidade.EntidadeConsultaService;
import br.com.ghnetsoft.pregao_presencial.core.entidade.preenchimento.EntidadePreenchimento;
import br.com.ghnetsoft.pregao_presencial.core.entidade.resource.EntidadeSimplesResource;

@Service
public class EntidadeConsultaServiceImpl implements EntidadeConsultaService {

	@Autowired
	private EntidadeRepository repository;

	@Autowired
	private EntidadePreenchimento entidadePreenchimento;

	@Override
	public Collection<EntidadeSimplesResource> todosAtivosPorUsuario(Long idUsuario) {
		Collection<Entidade> entidades = repository.todosAtivosPorUsuario(idUsuario);
		Collection<EntidadeSimplesResource> retorno = new ArrayList<>();
		entidades.forEach(entidade -> {
			EntidadeSimplesResource resource = EntidadeSimplesResource.builder()
					.cnpjNome(entidadePreenchimento.preenchimento(entidade.getCnpj(), entidade.getRazaoSocial()))
					.build();
			resource.setId(entidade.getId());
			retorno.add(resource);
		});
		return retorno;
	}
}
