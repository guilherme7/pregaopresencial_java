package br.com.ghnetsoft.pregao_presencial.core.cancelamentoanulacao;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.enuns.TipoCancelamentoAnulacaoEnum;
import br.com.ghnetsoft.pregao_presencial.core.pregao.Pregao;
import br.com.ghnetsoft.pregao_presencial.core.pregaoitem.PregaoItem;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_CANCELAMENTO_ANULACAO", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "FK_PREGAO", "ST_TIPO" }, name = "U1_TAB_CANCELAMENTO_ANULACAO"),
		@UniqueConstraint(columnNames = { "FK_PREGAO", "FK_PREGAO_ITEM",
				"ST_TIPO" }, name = "U2_TAB_CANCELAMENTO_ANULACAO") }, indexes = {
						@Index(columnList = "ST_REGISTRO", name = "I1_TAB_CANCELAMENTO_ANULACAO"),
						@Index(columnList = "FK_PREGAO", name = "I2_TAB_CANCELAMENTO_ANULACAO"),
						@Index(columnList = "FK_PREGAO_ITEM", name = "I3_TAB_CANCELAMENTO_ANULACAO"),
						@Index(columnList = "ST_TIPO", name = "I4_TAB_CANCELAMENTO_ANULACAO") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class CancelamentoAnulacao extends Principal {

	private static final long serialVersionUID = -3329255106735240373L;

	@Id
	@Column(name = "PK_CANCELAMENTO_ANULACAO")
	@SequenceGenerator(name = "SQ_CANCELAMENTO_ANULACAO", sequenceName = "SQ_CANCELAMENTO_ANULACAO", allocationSize = 1)
	@GeneratedValue(generator = "SQ_CANCELAMENTO_ANULACAO", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Pregao.class)
	@JoinColumn(name = "FK_PREGAO", nullable = false, foreignKey = @ForeignKey(name = "FK_PREGAO"))
	private Pregao pregao;
	@ManyToOne(targetEntity = PregaoItem.class)
	@JoinColumn(name = "FK_PREGAO_ITEM", foreignKey = @ForeignKey(name = "FK_ITEM"))
	private PregaoItem item;
	@Enumerated(STRING)
	@Column(name = "ST_TIPO", nullable = false, length = 15)
	private TipoCancelamentoAnulacaoEnum tipo;
	@Column(name = "DS_MOTIVO", nullable = false, length = 4000)
	private String motivo;
}
