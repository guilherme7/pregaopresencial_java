package br.com.ghnetsoft.pregao_presencial.core.compra.servico.grupo;

import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_SERVICO_GRUPO", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "NR_CODIGO" }, name = "U1_TAB_SERVICO_GRUPO") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_SERVICO_GRUPO"),
				@Index(columnList = "NR_CODIGO", name = "I2_TAB_SERVICO_GRUPO") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class ServicoGrupo extends Principal {

	private static final long serialVersionUID = 2342384121657694854L;

	@Id
	@Column(name = "PK_SERVICO_GRUPO")
	@SequenceGenerator(name = "SQ_SERVICO_GRUPO", sequenceName = "SQ_SERVICO_GRUPO", allocationSize = 1)
	@GeneratedValue(generator = "SQ_SERVICO_GRUPO", strategy = SEQUENCE)
	private Long id;
	@Column(name = "NR_CODIGO", nullable = false)
	private Integer codigo;
	@Column(name = "DS_DESCRICAO", nullable = false)
	private String descricao;
}
