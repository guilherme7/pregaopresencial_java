package br.com.ghnetsoft.pregao_presencial.core.pregao.resource;

import static lombok.AccessLevel.PROTECTED;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ghnetsoft.pregao_presencial.core.competencia.resource.CompetenciaSimplesRetornoResource;
import br.com.ghnetsoft.principal.core.resorce.EnumResource;
import br.com.ghnetsoft.principal.core.resorce.PrincipalRetornoIdResource;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Resource que retorna o pregao em pesquisa
 */
@Setter
@Getter
@Builder
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class PregaoPesquisaRetornoResource extends PrincipalRetornoIdResource {

	private static final long serialVersionUID = -5376333937643743944L;

	private PregaoSimplesRetornoResource licitacao;
	private CompetenciaSimplesRetornoResource competencia;
	private EnumResource fase;
	private EnumResource regime;
	private BigDecimal valorEstimado;
	private Long quantidadeItens;
	private BigDecimal valorPorLance;
	private Long quantidadeFornecedor;
	private Date abertura;
	private Date fechamento;
}
