package br.com.ghnetsoft.pregao_presencial.core.pessoa;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.entidade.Entidade;
import br.com.ghnetsoft.principal.core.enuns.PessoaTipoEnum;
import br.com.ghnetsoft.principal.core.enuns.PessoaTipoPessoaEnum;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_PESSOA", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "DS_DOCUMENTO", "FK_ENTIDADE" }, name = "U1_TAB_PESSOA") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_PESSOA"),
				@Index(columnList = "DS_DOCUMENTO", name = "I2_TAB_PESSOA"),
				@Index(columnList = "ST_TIPO", name = "I3_TAB_PESSOA"),
				@Index(columnList = "ST_TIPO_PESSOA", name = "I4_TAB_PESSOA") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class Pessoa extends Principal {

	private static final long serialVersionUID = -4989653216322812791L;

	@Id
	@Column(name = "PK_PESSOA")
	@SequenceGenerator(name = "SQ_PESSOA", sequenceName = "SQ_PESSOA", allocationSize = 1)
	@GeneratedValue(generator = "SQ_PESSOA", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Entidade.class)
	@JoinColumn(name = "FK_ENTIDADE", nullable = false, foreignKey = @ForeignKey(name = "FK_ENTIDADE"))
	private Entidade entidade;
	@Column(name = "DS_NOME_RAZAO_SOCIAL", nullable = false)
	private String nomeRazaoSocial;
	@Column(name = "DS_DOCUMENTO", nullable = false, length = 14)
	private String documento;
	@Enumerated(STRING)
	@Column(name = "ST_TIPO", nullable = false, length = 4)
	private PessoaTipoEnum tipo;
	@Enumerated(STRING)
	@Column(name = "ST_TIPO_PESSOA", nullable = false, length = 30)
	private PessoaTipoPessoaEnum tipoPessoa;
}
