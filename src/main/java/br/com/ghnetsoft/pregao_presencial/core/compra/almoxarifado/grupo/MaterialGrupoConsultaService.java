package br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.grupo;

import java.util.Collection;

import br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.grupo.resource.MaterialGrupoResource;

/**
 * @author Guilherme C Lopes
 * 
 * @version Interface que envia as informações de material grupo para consultar
 *          no banco de dados
 */
public interface MaterialGrupoConsultaService {

	/**
	 * @version Metodo que busca todos materiais por entidade
	 * 
	 * @param idEntidade
	 * @return
	 */
	Collection<MaterialGrupoResource> porEntidade(Long idEntidade);
}
