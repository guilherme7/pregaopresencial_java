package br.com.ghnetsoft.pregao_presencial.core.pregao.impl;

import java.math.BigDecimal;
import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.ghnetsoft.pregao_presencial.core.enuns.LicitacaoRegimeEnum;
import br.com.ghnetsoft.pregao_presencial.core.enuns.PregaoPregaoItemFasesEnum;
import br.com.ghnetsoft.pregao_presencial.core.pregao.Pregao;
import br.com.ghnetsoft.principal.core.enuns.StatusDoRegistroEnum;

@Repository
interface PregaoRepository extends JpaRepository<Pregao, Long> {

	@Query(value = "SELECT e FROM Pregao e WHERE e.licitacao.entidade IN "
			+ "(SELECT eu.entidade FROM EntidadeUsuario eu WHERE eu.usuario.id = :idUsuario) "
			+ "AND e.statusDoRegistro = 'ATIVO' ORDER BY e.licitacao.numero DESC, e.licitacao.ano ASC  ")
	Collection<Pregao> ativosPorUsuario(Long idUsuario);

	@Query(value = "SELECT e FROM Pregao e WHERE e.licitacao.entidade.id = :idEntidade "
			+ "AND (e.id = :idPregao or :idPregao is null) "
			+ "AND (e.competencia.id = :idCompetenciaPregao or :idCompetenciaPregao is null) "
			+ "AND (e.fase = :fasePregao or :fasePregao is null) "
			+ "AND (e.licitacao.regime = :regime or :regime is null) "
			+ "AND (e.licitacao.valorEstimado = :valorEstimado or :valorEstimado is null) "
			+ "AND (e.statusDoRegistro = :statusDoRegistro or :statusDoRegistro is null) "
			+ "AND (e.licitacao.competencia.id = :idCompetenciaLicitacao or :idCompetenciaLicitacao is null) ")
	Page<Pregao> paginacao(Long idEntidade, Long idPregao, Long idCompetenciaPregao,
			PregaoPregaoItemFasesEnum fasePregao, LicitacaoRegimeEnum regime, BigDecimal valorEstimado,
			StatusDoRegistroEnum statusDoRegistro, Long idCompetenciaLicitacao, Pageable pageable);
}
