package br.com.ghnetsoft.pregao_presencial.core.licitacaoitem;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.materialitem.MaterialItem;
import br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.unidade.MaterialUnidade;
import br.com.ghnetsoft.pregao_presencial.core.compra.lote.Lote;
import br.com.ghnetsoft.pregao_presencial.core.compra.servico.servico.Servico;
import br.com.ghnetsoft.pregao_presencial.core.entidade.Entidade;
import br.com.ghnetsoft.pregao_presencial.core.enuns.LicitacaoItemTipoEnum;
import br.com.ghnetsoft.pregao_presencial.core.licitacao.Licitacao;
import br.com.ghnetsoft.principal.core.enuns.SimNaoEnum;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_LICITACAO_ITEM", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "FK_LICITACAO", "FK_MATERIAL_UNIDADE",
				"FK_MATERIAL_ITEM" }, name = "U1_TAB_LICITACAO_ITEM"),
		@UniqueConstraint(columnNames = { "FK_LICITACAO", "FK_SERVICO" }, name = "U2_TAB_LICITACAO_ITEM"),
		@UniqueConstraint(columnNames = { "FK_LICITACAO", "FK_LOTE" }, name = "U3_TAB_LICITACAO_ITEM") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_LICITACAO_ITEM"),
				@Index(columnList = "FK_LICITACAO", name = "I2_TAB_LICITACAO_ITEM"),
				@Index(columnList = "FK_MATERIAL_UNIDADE", name = "I3_TAB_LICITACAO_ITEM"),
				@Index(columnList = "FK_MATERIAL_ITEM", name = "I4_TAB_LICITACAO_ITEM"),
				@Index(columnList = "FK_SERVICO", name = "I5_TAB_LICITACAO_ITEM"),
				@Index(columnList = "FK_LOTE", name = "I6_TAB_LICITACAO_ITEM"),
				@Index(columnList = "ST_TIPO_ITEM", name = "I7_TAB_LICITACAO_ITEM"),
				@Index(columnList = "ST_COMPROU", name = "I8_TAB_LICITACAO_ITEM") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class LicitacaoItem extends Principal {

	private static final long serialVersionUID = -3247297876543708992L;

	@Id
	@Column(name = "PK_LICITACAO_ITEM")
	@SequenceGenerator(name = "SQ_LICITACAO_ITEM", sequenceName = "SQ_LICITACAO_ITEM", allocationSize = 1)
	@GeneratedValue(generator = "SQ_LICITACAO_ITEM", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Licitacao.class)
	@JoinColumn(name = "FK_LICITACAO", nullable = false, foreignKey = @ForeignKey(name = "FK_LICITACAO"))
	private Licitacao licitacao;
	@Enumerated(STRING)
	@Column(name = "ST_TIPO_ITEM", nullable = false)
	private LicitacaoItemTipoEnum tipo;
	@Enumerated(STRING)
	@Column(name = "ST_COMPROU", nullable = false, length = 3)
	private SimNaoEnum comprou;
	@Column(name = "NR_QUANTIDADE", nullable = false, length = 25, precision = 2)
	private BigDecimal quantidade;
	@Column(name = "NR_NUMERO", nullable = false)
	private Integer numero;
	// Almoxarifado
	@ManyToOne(targetEntity = MaterialUnidade.class)
	@JoinColumn(name = "FK_MATERIAL_UNIDADE", foreignKey = @ForeignKey(name = "FK_MATERIAL_UNIDADE"))
	private MaterialUnidade MaterialUnidade;
	@ManyToOne(targetEntity = MaterialItem.class)
	@JoinColumn(name = "FK_MATERIAL_ITEM", foreignKey = @ForeignKey(name = "FK_MATERIAL_ITEM"))
	private MaterialItem materialItem;
	// Serviço
	@ManyToOne(targetEntity = Entidade.class)
	@JoinColumn(name = "FK_SERVICO", foreignKey = @ForeignKey(name = "FK_SERVICO"))
	private Servico servico;
	// Lote
	@ManyToOne(targetEntity = Lote.class)
	@JoinColumn(name = "FK_LOTE", foreignKey = @ForeignKey(name = "FK_LOTE"))
	private Lote lote;
}
