package br.com.ghnetsoft.pregao_presencial.core.usuario.impl;

import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ghnetsoft.pregao_presencial.core.usuario.Usuario;

/**
 * Interface para o banco de dados
 * 
 * @author Guilherme C Lopes
 */
@Repository
interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	String USERS_BY_LOGIN_CACHE = "usersByLogin";
	String USERS_BY_EMAIL_CACHE = "usersByEmail";

	/**
	 * Consulta para retornar o usuario por login
	 * 
	 * @param login
	 * @return
	 */
	@EntityGraph(attributePaths = "authorities")
	@Cacheable(cacheNames = USERS_BY_LOGIN_CACHE)
	Optional<Usuario> findByLogin(String login);

	/**
	 * Consulta para retornar o usuario por email
	 * 
	 * @param email
	 * @return
	 */
	@EntityGraph(attributePaths = "authorities")
	@Cacheable(cacheNames = USERS_BY_EMAIL_CACHE)
	Optional<Usuario> findByEmail(String email);
}
