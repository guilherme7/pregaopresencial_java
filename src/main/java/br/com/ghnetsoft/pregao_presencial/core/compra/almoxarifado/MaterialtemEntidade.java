package br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado;

import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.materialitem.MaterialItem;
import br.com.ghnetsoft.pregao_presencial.core.entidade.Entidade;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_MATERIAL_ITEM_EMPRESA", uniqueConstraints = { @UniqueConstraint(columnNames = { "FK_MATERIAL_ITEM",
		"FK_ENTIDADE" }, name = "U1_TAB_MATERIAL_ITEM_EMPRESA") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_MATERIAL_ITEM_EMPRESA"),
				@Index(columnList = "FK_MATERIAL_ITEM", name = "I2_TAB_MATERIAL_ITEM_EMPRESA"),
				@Index(columnList = "FK_ENTIDADE", name = "I3_TAB_MATERIAL_ITEM_EMPRESA") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class MaterialtemEntidade extends Principal {

	private static final long serialVersionUID = -418049334188090690L;

	@Id
	@Column(name = "PK_MATERIAL_ITEM_EMPRESA")
	@SequenceGenerator(name = "SQ_MATERIAL_ITEM_EMPRESA", sequenceName = "SQ_MATERIAL_ITEM_EMPRESA", allocationSize = 1)
	@GeneratedValue(generator = "SQ_MATERIAL_ITEM_EMPRESA", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Entidade.class)
	@JoinColumn(name = "FK_ENTIDADE", nullable = false, foreignKey = @ForeignKey(name = "FK_ENTIDADE"))
	private Entidade entidade;
	@ManyToOne(targetEntity = MaterialItem.class)
	@JoinColumn(name = "FK_MATERIAL_ITEM", nullable = false, foreignKey = @ForeignKey(name = "FK_MATERIAL_ITEM"))
	private MaterialItem materialItem;
}
