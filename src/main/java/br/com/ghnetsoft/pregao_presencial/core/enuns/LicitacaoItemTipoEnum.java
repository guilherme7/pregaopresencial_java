package br.com.ghnetsoft.pregao_presencial.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Enum do tipo de licitacao
 */
@Getter
public enum LicitacaoItemTipoEnum {

	MATERIAL("Material"), SERVICOS("Serviços"), LOTES("Lotes");

	private String descricao;

	/**
	 * @version construtor
	 * 
	 * @param descricao
	 */
	LicitacaoItemTipoEnum(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @version Busca o tipo de licitacao pelo valor
	 * 
	 * @param descricao
	 * @return
	 */
	public static LicitacaoItemTipoEnum buscaLicitacaoItemTipoEnum(String descricao) {
		for (LicitacaoItemTipoEnum enun : LicitacaoItemTipoEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}
}
