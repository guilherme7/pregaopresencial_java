package br.com.ghnetsoft.pregao_presencial.core.compra.servico.servico;

/**
 * @author Guilherme C Lopes
 * 
 * @version Interface que preenche o valor de um servico
 */
public interface ServicoPreenchimento {

	/**
	 * @version Metodo que preenche um servico
	 * 
	 * @param grupo
	 * @param codigo
	 * @param descricao
	 * @return
	 */
	String grupoCodigoDescricao(String grupo, String codigo, String descricao);
}
