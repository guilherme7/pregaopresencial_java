package br.com.ghnetsoft.pregao_presencial.core.exception;

import java.util.Collection;

import br.com.ghnetsoft.principal.utilitario.mensagem.Mensagem;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Excecao para a configuracao
 */
@Getter
@Setter
public class ConfiguracaoException extends RuntimeException {

	private static final long serialVersionUID = -8975652328932968130L;

	private Collection<Mensagem> mensagens;

	/**
	 * @version Insere os valores na excecao
	 * 
	 * @param mensagens
	 */
	public ConfiguracaoException(Collection<Mensagem> mensagens) {
		super();
		this.mensagens = mensagens;
	}
}
