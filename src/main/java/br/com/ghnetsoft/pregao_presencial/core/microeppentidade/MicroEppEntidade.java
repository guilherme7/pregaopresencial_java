package br.com.ghnetsoft.pregao_presencial.core.microeppentidade;

import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.pessoa.Pessoa;
import br.com.ghnetsoft.pregao_presencial.core.pregao.Pregao;
import br.com.ghnetsoft.pregao_presencial.core.pregaoitem.PregaoItem;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_MICRO_EPP_EMPRESA", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "FK_PESSOA", "FK_PREGAO", "VR_ATUAL",
				"VR_ULTIMO" }, name = "U1_TAB_MICRO_EPP_EMPRESA"),
		@UniqueConstraint(columnNames = { "FK_PESSOA", "FK_PREGAO", "FK_PREGAO_ITEM", "VR_ATUAL",
				"VR_ULTIMO" }, name = "U2_TAB_MICRO_EPP_EMPRESA") }, indexes = {
						@Index(columnList = "ST_REGISTRO", name = "I1_TAB_MICRO_EPP_EMPRESA"),
						@Index(columnList = "FK_PREGAO", name = "I2_TAB_MICRO_EPP_EMPRESA"),
						@Index(columnList = "FK_PREGAO_ITEM", name = "I3_TAB_MICRO_EPP_EMPRESA"),
						@Index(columnList = "FK_PESSOA", name = "I4_TAB_MICRO_EPP_EMPRESA") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class MicroEppEntidade extends Principal {

	private static final long serialVersionUID = 5827808030549603842L;

	@Id
	@Column(name = "PK_MICRO_EPP_EMPRESA")
	@SequenceGenerator(name = "SQ_MICRO_EPP_EMPRESA", sequenceName = "SQ_MICRO_EPP_EMPRESA", allocationSize = 1)
	@GeneratedValue(generator = "SQ_MICRO_EPP_EMPRESA", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Pregao.class)
	@JoinColumn(name = "FK_PREGAO", nullable = false, foreignKey = @ForeignKey(name = "FK_PREGAO"))
	private Pregao pregao;
	@ManyToOne(targetEntity = PregaoItem.class)
	@JoinColumn(name = "FK_PREGAO_ITEM", foreignKey = @ForeignKey(name = "FK_ITEM"))
	private PregaoItem item;
	@ManyToOne(targetEntity = Pessoa.class)
	@JoinColumn(name = "FK_PESSOA", nullable = false, foreignKey = @ForeignKey(name = "FK_PESSOA"))
	private Pessoa pessoa;
	@Column(name = "VR_ATUAL", nullable = false, length = 25, precision = 2)
	private BigDecimal atual;
	@Column(name = "VR_ULTIMO", nullable = false, length = 25, precision = 2)
	private BigDecimal ultimo;
}
