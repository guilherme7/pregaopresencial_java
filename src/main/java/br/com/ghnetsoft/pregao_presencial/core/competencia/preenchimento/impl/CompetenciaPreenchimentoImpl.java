package br.com.ghnetsoft.pregao_presencial.core.competencia.preenchimento.impl;

import org.springframework.stereotype.Component;

import br.com.ghnetsoft.pregao_presencial.core.competencia.preenchimento.CompetenciaPreenchimento;

@Component
public class CompetenciaPreenchimentoImpl implements CompetenciaPreenchimento {

	@Override
	public String preenchimento(String mes, Integer ano) {
		return mes + " / " + ano.toString();
	}
}
