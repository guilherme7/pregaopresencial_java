package br.com.ghnetsoft.pregao_presencial.core.competencia;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.entidade.Entidade;
import br.com.ghnetsoft.pregao_presencial.core.enuns.CompetenciaTipoEnum;
import br.com.ghnetsoft.principal.core.enuns.MesEnum;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_COMPETENCIA", uniqueConstraints = { @UniqueConstraint(columnNames = { "FK_ENTIDADE", "NR_ANO",
		"ST_MES", "ST_TIPO" }, name = "U1_TAB_COMPETENCIA") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_COMPETENCIA"),
				@Index(columnList = "FK_ENTIDADE", name = "I2_TAB_COMPETENCIA"),
				@Index(columnList = "NR_ANO", name = "I3_TAB_COMPETENCIA"),
				@Index(columnList = "ST_MES", name = "I4_TAB_COMPETENCIA"),
				@Index(columnList = "ST_TIPO", name = "I5_TAB_COMPETENCIA") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class Competencia extends Principal {

	private static final long serialVersionUID = -7543338835832489467L;

	@Id
	@Column(name = "PK_COMPETENCIA")
	@SequenceGenerator(name = "SQ_COMPETENCIA", sequenceName = "SQ_COMPETENCIA", allocationSize = 1)
	@GeneratedValue(generator = "SQ_COMPETENCIA", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Entidade.class)
	@JoinColumn(name = "FK_ENTIDADE", nullable = false, foreignKey = @ForeignKey(name = "FK_ENTIDADE"))
	private Entidade entidade;
	@Column(name = "NR_ANO", nullable = false)
	private Integer ano;
	@Enumerated(STRING)
	@Column(name = "ST_MES", nullable = false, length = 10)
	private MesEnum mes;
	@Enumerated(STRING)
	@Column(name = "ST_TIPO", nullable = false, length = 15)
	private CompetenciaTipoEnum tipo;
}
