package br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.materialitem;

/**
 * @author Guilherme C Lopes
 * 
 * @version Interface que preenche o valor de um item do material
 */
public interface MaterialItemPreenchimento {

	/**
	 * @version Metodo que preenche um item do material
	 * 
	 * @param federalSulpply
	 * @param descricao
	 * @param unidade
	 * @return
	 */
	String federalSulpplyNomeUnidade(String federalSulpply, String descricao, String unidade);
}
