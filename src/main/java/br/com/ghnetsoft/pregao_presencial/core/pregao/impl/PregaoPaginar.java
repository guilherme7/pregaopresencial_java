package br.com.ghnetsoft.pregao_presencial.core.pregao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import br.com.ghnetsoft.pregao_presencial.core.pregao.Pregao;
import br.com.ghnetsoft.pregao_presencial.core.pregao.resource.PregaoPesquisaEnvioResource;
import br.com.ghnetsoft.principal.core.resorce.PaginacaoRetornoResource;
import br.com.ghnetsoft.principal.core.service.PaginacaoService;

@Component
class PregaoPaginar extends PaginacaoService {

	PaginacaoRetornoResource paginacao(PregaoPesquisaEnvioResource resource, Page<Pregao> entidades) {
		Map<String, Object> parametros = new HashMap<>();
		parametros(resource, parametros);
		preencherCampoParametro(true, resource.getIdEntidade(), "idEntidade", parametros);
		return new PaginacaoRetornoResource(entidades.getNumberOfElements(), entidades.hasContent(),
				entidades.hasNext(), entidades.hasPrevious(), entidades.isFirst(), entidades.isLast(),
				mensagemRetornoPesquisa(!entidades.isEmpty()), paginacaoProximo(entidades), entidades.getNumber(),
				paginacaoAnterior(entidades), entidades.getSize(), parametros,
				ajustaCampoOrdenado(resource.getDirecao(), resource.getCampo()), entidades.getTotalElements(),
				entidades.getTotalPages());
	}

	private void parametros(PregaoPesquisaEnvioResource resource, Map<String, Object> parametros) {
	}
}
