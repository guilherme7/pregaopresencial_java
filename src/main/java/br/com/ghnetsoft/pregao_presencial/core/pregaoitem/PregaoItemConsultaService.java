package br.com.ghnetsoft.pregao_presencial.core.pregaoitem;

public interface PregaoItemConsultaService {

	Long contarQuantidadeItensPorPregao(Long idPregao);
}
