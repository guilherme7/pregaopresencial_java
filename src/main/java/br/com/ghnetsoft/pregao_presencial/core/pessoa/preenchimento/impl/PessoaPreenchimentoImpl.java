package br.com.ghnetsoft.pregao_presencial.core.pessoa.preenchimento.impl;

import static br.com.ghnetsoft.principal.utilitario.validacao.CpfCnpjUtils.mascararToCnpjCpf;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import org.springframework.stereotype.Component;

import br.com.ghnetsoft.pregao_presencial.core.pessoa.preenchimento.PessoaPreenchimento;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe que preenche o valor de uma pessoa
 */
@Component
public class PessoaPreenchimentoImpl implements PessoaPreenchimento {

	/**
	 * @version Metodo que preenche uma pessoa
	 * 
	 * @param nomeRazaoSocial
	 * @param documento
	 * @return
	 */
	@Override
	public String preenchimento(String nomeRazaoSocial, String documento) {
		if (isNotEmpty(documento) && isNotEmpty(nomeRazaoSocial)) {
			return mascararToCnpjCpf(documento) + " - " + nomeRazaoSocial;
		} else if (isNotEmpty(nomeRazaoSocial)) {
			return nomeRazaoSocial;
		}
		return null;
	}
}
