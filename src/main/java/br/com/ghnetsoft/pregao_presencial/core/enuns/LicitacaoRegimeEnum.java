package br.com.ghnetsoft.pregao_presencial.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Enum do regime da licitacao
 */
@Getter
public enum LicitacaoRegimeEnum {

	MENOR_PRECO_GLOBAL("Menor preço global"), MENOR_PRECO_POR_ITEM("Menor preço por item"),
	MENOR_PRECO_POR_LOTE("Menor preço por lote");

	private String descricao;

	/**
	 * @version construtor
	 * 
	 * @param descricao
	 */
	LicitacaoRegimeEnum(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @version Busca o regime da licitacao pelo valor
	 * 
	 * @param descricao
	 * @return
	 */
	public static LicitacaoRegimeEnum buscaLicitacaoRegimeEnum(String descricao) {
		for (LicitacaoRegimeEnum enun : LicitacaoRegimeEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}
}
