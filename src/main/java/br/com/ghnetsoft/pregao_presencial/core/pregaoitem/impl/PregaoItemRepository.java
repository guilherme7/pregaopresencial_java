package br.com.ghnetsoft.pregao_presencial.core.pregaoitem.impl;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.ghnetsoft.pregao_presencial.core.pregaoitem.PregaoItem;

@Repository
interface PregaoItemRepository extends JpaRepository<PregaoItem, Long> {

	@Query(value = "SELECT COUNT(e.id) FROM PregaoItem e WHERE e.pregao.id = :idPregao ")
	Long contarQuantidadeItensPorPregao(Long idPregao);
}
