package br.com.ghnetsoft.pregao_presencial.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Enum do tipo de lote
 */
@Getter
public enum LoteTipoEnum {

	MATERIAL("Material"), SERVICOS("Serviços"), LOTES_SERVICOS("Materiais e serviços");

	private String descricao;

	/**
	 * @version construtor
	 * 
	 * @param descricao
	 */
	LoteTipoEnum(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @version Busca o tipo de lote pelo valor
	 * 
	 * @param descricao
	 * @return
	 */
	public static LoteTipoEnum buscaLoteTipoEnum(String descricao) {
		for (LoteTipoEnum enun : LoteTipoEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}
}
