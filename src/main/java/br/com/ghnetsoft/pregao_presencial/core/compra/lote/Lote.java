package br.com.ghnetsoft.pregao_presencial.core.compra.lote;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.compra.lote.enuns.LoteTipoEnum;
import br.com.ghnetsoft.pregao_presencial.core.entidade.Entidade;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_LOTE", uniqueConstraints = { @UniqueConstraint(columnNames = { "NR_CODIGO", "FK_ENTIDADE",
		"ST_TIPO_LOTE" }, name = "U1_TAB_LOTE") }, indexes = { @Index(columnList = "ST_REGISTRO", name = "I1_TAB_LOTE"),
				@Index(columnList = "FK_ENTIDADE", name = "I2_TAB_LOTE"),
				@Index(columnList = "NR_CODIGO", name = "I3_TAB_LOTE"),
				@Index(columnList = "ST_TIPO_LOTE", name = "I4_TAB_LOTE") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class Lote extends Principal {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PK_LOTE")
	@SequenceGenerator(name = "SQ_LOTE", sequenceName = "SQ_LOTE", allocationSize = 1)
	@GeneratedValue(generator = "SQ_LOTE", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Entidade.class)
	@JoinColumn(name = "FK_ENTIDADE", nullable = false, foreignKey = @ForeignKey(name = "FK_ENTIDADE"))
	private Entidade entidade;
	@Column(name = "NR_CODIGO", nullable = false)
	private Integer codigo;
	@Column(name = "DS_DESCRICAO", nullable = false)
	private String descricao;
	@Enumerated(STRING)
	@Column(name = "ST_TIPO_LOTE", nullable = false, length = 15)
	private LoteTipoEnum tipo;
}
