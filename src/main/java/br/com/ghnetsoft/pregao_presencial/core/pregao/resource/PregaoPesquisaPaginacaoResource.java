package br.com.ghnetsoft.pregao_presencial.core.pregao.resource;

import static lombok.AccessLevel.PROTECTED;

import java.util.Collection;

import br.com.ghnetsoft.principal.core.resorce.PaginacaoRetornoResource;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Resource que retorna a paginacao de um pregao
 */
@Setter
@Getter
@Builder
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class PregaoPesquisaPaginacaoResource {

	private Collection<PregaoPesquisaRetornoResource> lista;
	private PaginacaoRetornoResource paginacao;
}
