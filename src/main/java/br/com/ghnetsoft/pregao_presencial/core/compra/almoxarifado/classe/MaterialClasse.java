package br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.classe;

import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.grupo.MaterialGrupo;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_MATERIAL_CLASSE", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "FK_MATERIAL_GRUPO", "DS_CODIGO" }, name = "U1_TAB_MATERIAL_CLASSE"),
		@UniqueConstraint(columnNames = { "FK_MATERIAL_GRUPO", "DS_CODIGO",
				"DS_DESCRICAO" }, name = "U2_TAB_MATERIAL_CLASSE") }, indexes = {
						@Index(columnList = "ST_REGISTRO", name = "I1_TAB_MATERIAL_CLASSE"),
						@Index(columnList = "FK_MATERIAL_GRUPO", name = "I2_TAB_MATERIAL_CLASSE"),
						@Index(columnList = "DS_CODIGO", name = "I3_TAB_MATERIAL_CLASSE") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class MaterialClasse extends Principal {

	private static final long serialVersionUID = -4911735962296594404L;

	@Id
	@Column(name = "PK_MATERIAL_CLASSE")
	@SequenceGenerator(name = "SQ_MATERIAL_CLASSE", sequenceName = "SQ_MATERIAL_CLASSE", allocationSize = 1)
	@GeneratedValue(generator = "SQ_MATERIAL_CLASSE", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = MaterialGrupo.class)
	@JoinColumn(name = "FK_MATERIAL_GRUPO", nullable = false, foreignKey = @ForeignKey(name = "FK_MATERIAL_GRUPO"))
	private MaterialGrupo grupo;
	@Column(name = "DS_CODIGO", nullable = false, length = 2)
	private String codigo;
	@Column(name = "DS_DESCRICAO", nullable = false)
	private String descricao;
}
