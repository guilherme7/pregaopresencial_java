package br.com.ghnetsoft.pregao_presencial.core.entidade.preenchimento.impl;

import static br.com.ghnetsoft.principal.utilitario.validacao.CpfCnpjUtils.mascararToCnpjCpf;

import org.springframework.stereotype.Component;

import br.com.ghnetsoft.pregao_presencial.core.entidade.preenchimento.EntidadePreenchimento;

@Component
public class EntidadePreenchimentoImpl implements EntidadePreenchimento {

	@Override
	public String preenchimento(String cnpj, String razaoSocial) {
		return mascararToCnpjCpf(cnpj) + " - " + razaoSocial;
	}
}
