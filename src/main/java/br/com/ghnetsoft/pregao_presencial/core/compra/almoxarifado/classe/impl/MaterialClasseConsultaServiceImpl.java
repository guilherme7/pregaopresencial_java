package br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.classe.impl;

import java.util.Collection;

import br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.classe.MaterialClasseConsultaService;
import br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.classe.resource.MaterialClasseResource;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe que envia as informações de material classe para consultar no
 *          banco de dados
 */
public class MaterialClasseConsultaServiceImpl implements MaterialClasseConsultaService {

	@Override
	public Collection<MaterialClasseResource> buscarTodos() {
		return null;
	}
}
