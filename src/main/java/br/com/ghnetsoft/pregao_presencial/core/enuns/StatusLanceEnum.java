package br.com.ghnetsoft.pregao_presencial.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Enum do satus do lance
 */
@Getter
public enum StatusLanceEnum {

	CONTINUA("Continua"), INABILITADO("Inabilitado"), DESCLASSIFICADO("Desclassificado"), PAROU("Parou de dar lances");

	private String descricao;

	/**
	 * @version construtor
	 * 
	 * @param descricao
	 */
	StatusLanceEnum(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @version Busca o status do lance pelo valor
	 * 
	 * @param descricao
	 * @return
	 */
	public static StatusLanceEnum buscaStatusLanceEnum(String descricao) {
		for (StatusLanceEnum enun : StatusLanceEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}
}
