package br.com.ghnetsoft.pregao_presencial.core.exception;

import java.util.Collection;

import br.com.ghnetsoft.principal.utilitario.mensagem.Mensagem;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Excecao para o item do pregao
 */
@Getter
@Setter
public class PregaoItemException extends RuntimeException {

	private static final long serialVersionUID = -8106601378987130241L;

	private Collection<Mensagem> mensagens;

	/**
	 * @version Insere os valores na excecao
	 * 
	 * @param mensagens
	 */
	public PregaoItemException(Collection<Mensagem> mensagens) {
		super();
		this.mensagens = mensagens;
	}
}
