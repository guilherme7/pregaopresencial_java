package br.com.ghnetsoft.pregao_presencial.core.lance;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.enuns.StatusLanceEnum;
import br.com.ghnetsoft.pregao_presencial.core.pessoa.Pessoa;
import br.com.ghnetsoft.pregao_presencial.core.pregao.Pregao;
import br.com.ghnetsoft.pregao_presencial.core.pregaoitem.PregaoItem;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_LANCE", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "FK_PESSOA", "FK_PREGAO", "FK_PREGAO_ITEM",
				"NR_RODADA" }, name = "U1_TAB_LANCE"),
		@UniqueConstraint(columnNames = { "FK_PESSOA", "FK_PREGAO_ITEM",
				"NR_RODADA" }, name = "U2_TAB_LANCE") }, indexes = {
						@Index(columnList = "ST_REGISTRO", name = "I1_TAB_LANCE"),
						@Index(columnList = "FK_PREGAO", name = "I2_TAB_LANCE"),
						@Index(columnList = "FK_PREGAO_ITEM", name = "I3_TAB_LANCE"),
						@Index(columnList = "FK_PESSOA", name = "I4_TAB_LANCE"),
						@Index(columnList = "NR_RODADA", name = "I5_TAB_LANCE"),
						@Index(columnList = "ST_STATUS", name = "I6_TAB_LANCE") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class Lance extends Principal {

	private static final long serialVersionUID = -8608706902145928273L;

	@Id
	@Column(name = "PK_LANCE")
	@SequenceGenerator(name = "SQ_LANCE", sequenceName = "SQ_LANCE", allocationSize = 1)
	@GeneratedValue(generator = "SQ_LANCE", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Pregao.class)
	@JoinColumn(name = "FK_PREGAO", nullable = false, foreignKey = @ForeignKey(name = "FK_PREGAO"))
	private Pregao pregao;
	@ManyToOne(targetEntity = PregaoItem.class)
	@JoinColumn(name = "FK_PREGAO_ITEM", foreignKey = @ForeignKey(name = "FK_ITEM"))
	private PregaoItem item;
	@ManyToOne(targetEntity = Pessoa.class)
	@JoinColumn(name = "FK_PESSOA", nullable = false, foreignKey = @ForeignKey(name = "FK_PESSOA"))
	private Pessoa pessoa;
	@Column(name = "NR_RODADA", nullable = false)
	private Integer rodada;
	@Column(name = "VL_LANCE", nullable = false, length = 25, precision = 2)
	private BigDecimal valor;
	@Enumerated(STRING)
	@Column(name = "ST_STATUS", nullable = false, length = 20)
	private StatusLanceEnum status;
}
