package br.com.ghnetsoft.pregao_presencial.core.configuracao;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.entidade.Entidade;
import br.com.ghnetsoft.principal.core.enuns.SimNaoEnum;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_CONFIGURACAO", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "FK_ENTIDADE" }, name = "U1_TAB_CONFIGURACAO") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_CONFIGURACAO"),
				@Index(columnList = "FK_ENTIDADE", name = "I2_TAB_CONFIGURACAO"),
				@Index(columnList = "ST_BANCO_LOCAL", name = "I3_TAB_CONFIGURACAO") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class Configuracao extends Principal {

	private static final long serialVersionUID = 2524291860069947075L;

	@Id
	@Column(name = "PK_CONFIGURACAO")
	@SequenceGenerator(name = "SQ_CONFIGURACAO", sequenceName = "SQ_CONFIGURACAO", allocationSize = 1)
	@GeneratedValue(generator = "SQ_CONFIGURACAO", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Entidade.class)
	@JoinColumn(name = "FK_ENTIDADE", nullable = false, foreignKey = @ForeignKey(name = "FK_ENTIDADE"))
	private Entidade entidade;
	@Column(name = "VR_PORCENTAGEM_MP_EPP", nullable = false, length = 25, precision = 2)
	private BigDecimal porcentagemMpEpp;
	@Column(name = "NR_TEMPO_MP_EPP", nullable = false)
	private Integer tempoMpEpp;
	@Column(name = "NR_QUANTIDADE_FORNECEDORES", nullable = false)
	private Integer quantidadeFornecedores;
	@Enumerated(STRING)
	@Column(name = "ST_BANCO_LOCAL", nullable = false, length = 3)
	private SimNaoEnum bancoLocal;
}
