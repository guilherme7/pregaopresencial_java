package br.com.ghnetsoft.pregao_presencial.core.compra.lote.servico;

import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.compra.lote.Lote;
import br.com.ghnetsoft.pregao_presencial.core.compra.servico.servico.Servico;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_LOTE_SERVICO", uniqueConstraints = { @UniqueConstraint(columnNames = { "FK_LOTE", "FK_SERVICO",
		"NR_QUANTIDADE" }, name = "U1_TAB_LOTE_SERVICO") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_LOTE_SERVICO"),
				@Index(columnList = "FK_LOTE", name = "I2_TAB_LOTE_SERVICO"),
				@Index(columnList = "FK_SERVICO", name = "I2_TAB_LOTE_SERVICO") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class LoteServico extends Principal {

	private static final long serialVersionUID = -165697106840260820L;

	@Id
	@Column(name = "PK_LOTE_SERVICO")
	@SequenceGenerator(name = "SQ_LOTE_SERVICO", sequenceName = "SQ_LOTE_SERVICO", allocationSize = 1)
	@GeneratedValue(generator = "SQ_LOTE_SERVICO", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Lote.class)
	@JoinColumn(name = "FK_LOTE", nullable = false, foreignKey = @ForeignKey(name = "FK_LOTE"))
	private Lote lote;
	@ManyToOne(targetEntity = Servico.class)
	@JoinColumn(name = "FK_SERVICO", nullable = false, foreignKey = @ForeignKey(name = "FK_SERVICO"))
	private Servico servico;
	@Column(name = "NR_QUANTIDADE", length = 25, precision = 2)
	private BigDecimal quantidade;
}
