package br.com.ghnetsoft.pregao_presencial.core.usuario;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;
import static org.hibernate.annotations.CacheConcurrencyStrategy.NONSTRICT_READ_WRITE;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cache;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.ghnetsoft.pregao_presencial.core.usuariorole.UsuarioRole;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_USUARIO", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "DS_LOGIN" }, name = "U1_TAB_USUARIO") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_USUARIO"),
				@Index(columnList = "DS_LOGIN", name = "I2_TAB_USUARIO") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class Usuario extends Principal {

	private static final long serialVersionUID = 6841912833042502417L;

	@Id
	@Column(name = "PK_USUARIO")
	@SequenceGenerator(name = "SQ_USUARIO", sequenceName = "SQ_USUARIO", allocationSize = 1)
	@GeneratedValue(generator = "SQ_USUARIO", strategy = SEQUENCE)
	private Long id;
	@JsonIgnore
	@Column(name = "DS_LOGIN", nullable = false)
	private String login;
	@JsonIgnore
	@Column(name = "DS_EMAIL", nullable = false)
	private String email;
	@JsonIgnore
	@Column(name = "DS_SENHA", nullable = false)
	private String senha;
	@JsonIgnore
	@NotNull
	@Column(nullable = false)
	private boolean activated;
	@JsonIgnore
	@OneToMany(mappedBy = "usuario", fetch = EAGER, targetEntity = UsuarioRole.class)
	@Cache(usage = NONSTRICT_READ_WRITE)
	@BatchSize(size = 20)
	private Collection<UsuarioRole> usuariosRoles;
}
