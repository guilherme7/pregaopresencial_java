package br.com.ghnetsoft.pregao_presencial.core.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * @author Guilherme C Lopes
 * 
 * @version Excecao para usuario não ativo
 */
public class UsuarioNaoAtivoException extends AuthenticationException {

	private static final long serialVersionUID = 6782746080366010450L;

	public UsuarioNaoAtivoException(String msg) {
		super(msg);
	}

	public UsuarioNaoAtivoException(String message, Throwable t) {
		super(message, t);
	}
}
