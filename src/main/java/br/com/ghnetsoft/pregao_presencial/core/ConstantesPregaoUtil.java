package br.com.ghnetsoft.pregao_presencial.core;

import lombok.experimental.UtilityClass;

/**
 * @author Guilherme C Lopes
 * 
 * @version Constantes do sistema de pregao presencial
 */
@UtilityClass
public class ConstantesPregaoUtil {

	public static final String LICITACAO_ERROR = "licitacao-error-";
	public static final String LICITACAO_ITEM_ERROR = "licitacao-item-error-";
	public static final String PREGAO_ITEM_ERROR = "pregao-item-error-";
	public static final String PREGAO_CLASSIFICACAO_INICIAL = "pregao-classificacao-inicial-";
	public static final String PREGAO_ITEM_CLASSIFICACAO_INICIAL = "pregao-item-classificacao-inicial-";
	public static final String COMPETENCIA_ERROR = "competencia-error-";
	public static final String ENTIDADE_ERROR = "entidade-error-";
	public static final String CONFIGURACAO_ERROR = "configuracao-error-";
	public static final String PESSOA_ERROR = "pessoa-error-";
	public static final String PARTICIPANTE_ERROR = "participante-error-";
	public static final String USUARIO_ERROR = "usuario-error-";
	
}
