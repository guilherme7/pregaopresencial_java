package br.com.ghnetsoft.pregao_presencial.core.pregao;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.SEQUENCE;
import static javax.persistence.TemporalType.TIMESTAMP;
import static lombok.AccessLevel.PROTECTED;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;

import br.com.ghnetsoft.pregao_presencial.core.competencia.Competencia;
import br.com.ghnetsoft.pregao_presencial.core.enuns.PregaoPregaoItemFasesEnum;
import br.com.ghnetsoft.pregao_presencial.core.licitacao.Licitacao;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_PREGAO", indexes = { @Index(columnList = "ST_REGISTRO", name = "I1_TAB_PREGAO"),
		@Index(columnList = "FK_LICITACAO", name = "I2_TAB_PREGAO"),
		@Index(columnList = "FK_COMPETENCIA", name = "I3_TAB_PREGAO") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class Pregao extends Principal {

	private static final long serialVersionUID = 1694781750287949024L;

	@Id
	@Column(name = "PK_PREGAO")
	@SequenceGenerator(name = "SQ_PREGAO", sequenceName = "SQ_PREGAO", allocationSize = 1)
	@GeneratedValue(generator = "SQ_PREGAO", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = Competencia.class)
	@JoinColumn(name = "FK_COMPETENCIA", nullable = false, foreignKey = @ForeignKey(name = "FK_COMPETENCIA"))
	private Competencia competencia;
	@ManyToOne(targetEntity = Licitacao.class)
	@JoinColumn(name = "FK_LICITACAO", nullable = false, foreignKey = @ForeignKey(name = "FK_LICITACAO"))
	private Licitacao licitacao;
	@Enumerated(STRING)
	@Column(name = "ST_FASE", nullable = false, length = 20)
	private PregaoPregaoItemFasesEnum fase;
	@Column(name = "VR_MINIMO_POR_LANCE", length = 25, precision = 2)
	private BigDecimal valorMinimoPorLance;
	@Column(name = "NR_QUANTIDADE_FORNECEDOR")
	private Long quantidadeFornecedor;
	@Temporal(TIMESTAMP)
	@Column(name = "TS_DATA_HORA_ABERTURA", nullable = false)
	private Date dataHoraAbertura;
	@Temporal(TIMESTAMP)
	@Column(name = "TS_DATA_HORA_FECHOU")
	private Date dataHoraFechou;
}
