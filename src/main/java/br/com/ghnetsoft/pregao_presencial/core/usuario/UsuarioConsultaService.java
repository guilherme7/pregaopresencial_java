package br.com.ghnetsoft.pregao_presencial.core.usuario;

import java.util.Optional;

/**
 * Interface que envia as informacoes de usuario para consultar no banco de
 * dados
 * 
 * @author Guilherme C Lopes
 */
public interface UsuarioConsultaService {

	/**
	 * Metodo para retornar o usuario por login
	 * 
	 * @param login
	 * @return
	 */
	Optional<Usuario> buscarPeloLogin(String login);

	/**
	 * Metodo para retornar o usuario por email
	 * 
	 * @param login
	 * @return
	 */
	Optional<Usuario> buscarPeloEmail(String email);
}
