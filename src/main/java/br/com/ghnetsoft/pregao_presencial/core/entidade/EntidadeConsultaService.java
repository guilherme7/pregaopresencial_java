package br.com.ghnetsoft.pregao_presencial.core.entidade;

import java.util.Collection;

import br.com.ghnetsoft.pregao_presencial.core.entidade.resource.EntidadeSimplesResource;

public interface EntidadeConsultaService {

	Collection<EntidadeSimplesResource> todosAtivosPorUsuario(Long idUsuario);
}
