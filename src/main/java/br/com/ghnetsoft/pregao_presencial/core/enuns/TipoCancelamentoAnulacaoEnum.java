package br.com.ghnetsoft.pregao_presencial.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Enum do tipo de cancelamento ou anulacao
 */
@Getter
public enum TipoCancelamentoAnulacaoEnum {

	ANULACAO("Anulação"), CANCELAMENTO("Cancelamento");

	private String descricao;

	/**
	 * @version construtor
	 * 
	 * @param descricao
	 */
	TipoCancelamentoAnulacaoEnum(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @version Busca o tipo de cancelamento ou anulacao pelo valor
	 * 
	 * @param descricao
	 * @return
	 */
	public static TipoCancelamentoAnulacaoEnum buscaTipoEnum(String descricao) {
		for (TipoCancelamentoAnulacaoEnum enun : TipoCancelamentoAnulacaoEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}
}
