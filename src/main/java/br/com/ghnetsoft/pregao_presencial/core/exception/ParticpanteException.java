package br.com.ghnetsoft.pregao_presencial.core.exception;

import java.util.Collection;

import br.com.ghnetsoft.principal.utilitario.mensagem.Mensagem;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Excecao para o participante do item do pregao
 */
@Getter
@Setter
public class ParticpanteException extends RuntimeException {

	private static final long serialVersionUID = 2138558590277935647L;

	private Collection<Mensagem> mensagens;

	/**
	 * @version Insere os valores na excecao
	 * 
	 * @param mensagens
	 */
	public ParticpanteException(Collection<Mensagem> mensagens) {
		super();
		this.mensagens = mensagens;
	}
}
