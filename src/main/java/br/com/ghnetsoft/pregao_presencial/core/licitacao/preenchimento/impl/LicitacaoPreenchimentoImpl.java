package br.com.ghnetsoft.pregao_presencial.core.licitacao.preenchimento.impl;

import static org.apache.commons.lang3.StringUtils.leftPad;

import org.springframework.stereotype.Component;

import br.com.ghnetsoft.pregao_presencial.core.licitacao.preenchimento.LicitacaoPreenchimento;

/**
 * @author Guilherme C Lopes
 * 
 * @version Interface que preenche o valor de uma licitacao
 */
@Component
public class LicitacaoPreenchimentoImpl implements LicitacaoPreenchimento {

	/**
	 * @version Metodo que preenche uma licitacao
	 * 
	 * @param numero
	 * @param ano
	 * @return
	 */
	@Override
	public String preenchimento(Integer numero, Integer ano) {
		return "PP: " + leftPad(numero.toString(), 4, "0") + " - " + ano.toString();
	}
}
