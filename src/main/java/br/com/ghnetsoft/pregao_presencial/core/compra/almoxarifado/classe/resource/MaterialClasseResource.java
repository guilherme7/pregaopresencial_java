package br.com.ghnetsoft.pregao_presencial.core.compra.almoxarifado.classe.resource;

import static lombok.AccessLevel.PROTECTED;

import br.com.ghnetsoft.principal.core.resorce.PrincipalRetornoIdResource;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Resoure que retorna grupo, codigo e descricao do material classe
 */
@Setter
@Getter
@Builder
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class MaterialClasseResource extends PrincipalRetornoIdResource {

	private static final long serialVersionUID = -7505997102915067231L;

	private String grupo;
	private String codigo;
	private String descricao;
}
