package br.com.ghnetsoft.pregao_presencial.core.pregao.resource;

import static lombok.AccessLevel.PROTECTED;

import br.com.ghnetsoft.principal.core.resorce.PrincipalRetornoIdResource;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class PregaoSimplesRetornoResource extends PrincipalRetornoIdResource {

	private static final long serialVersionUID = -1629891526386017690L;

	private String pregaoEntidade;
}
