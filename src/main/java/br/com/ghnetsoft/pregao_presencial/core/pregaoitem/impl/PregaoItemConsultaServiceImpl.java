package br.com.ghnetsoft.pregao_presencial.core.pregaoitem.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ghnetsoft.pregao_presencial.core.pregaoitem.PregaoItemConsultaService;

@Service
public class PregaoItemConsultaServiceImpl implements PregaoItemConsultaService {

	@Autowired
	private PregaoItemRepository repository;

	@Override
	public Long contarQuantidadeItensPorPregao(Long idPregao) {
		return repository.contarQuantidadeItensPorPregao(idPregao);
	}
}
