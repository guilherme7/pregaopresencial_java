package br.com.ghnetsoft.pregao_presencial.core.licitacao;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.SEQUENCE;
import static javax.persistence.TemporalType.TIMESTAMP;
import static lombok.AccessLevel.PROTECTED;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;

import br.com.ghnetsoft.pregao_presencial.core.competencia.Competencia;
import br.com.ghnetsoft.pregao_presencial.core.entidade.Entidade;
import br.com.ghnetsoft.pregao_presencial.core.enuns.LicitacaoRegimeEnum;
import br.com.ghnetsoft.pregao_presencial.core.licitacaoitem.LicitacaoItem;
import br.com.ghnetsoft.pregao_presencial.core.licitacaomodalidade.LicitacaoModalidade;
import br.com.ghnetsoft.pregao_presencial.core.pregao.Pregao;
import br.com.ghnetsoft.principal.core.enuns.SimNaoEnum;
import br.com.ghnetsoft.principal.core.modelo.Principal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 * 
 * @version Classe do banco de dados
 */
@Entity
@Setter
@Getter
@Builder
@Table(name = "TAB_LICITACAO", uniqueConstraints = { @UniqueConstraint(columnNames = { "NR_ANO", "NR_NUMERO",
		"FK_LICITACAO_MODALIDADE" }, name = "U1_TAB_LICITACAO") }, indexes = {
				@Index(columnList = "ST_REGISTRO", name = "I1_TAB_LICITACAO"),
				@Index(columnList = "NR_NUMERO", name = "I2_TAB_LICITACAO"),
				@Index(columnList = "NR_ANO", name = "I3_TAB_LICITACAO"),
				@Index(columnList = "ST_REGIME", name = "I4_TAB_LICITACAO"),
				@Index(columnList = "FK_LICITACAO_MODALIDADE", name = "I5_TAB_LICITACAO"),
				@Index(columnList = "FK_COMPETENCIA", name = "I6_TAB_LICITACAO"),
				@Index(columnList = "FK_ENTIDADE", name = "I7_TAB_LICITACAO") })
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class Licitacao extends Principal {

	private static final long serialVersionUID = 2387024141832638669L;

	@Id
	@Column(name = "PK_LICITACAO")
	@SequenceGenerator(name = "SQ_LICITACAO", sequenceName = "SQ_LICITACAO", allocationSize = 1)
	@GeneratedValue(generator = "SQ_LICITACAO", strategy = SEQUENCE)
	private Long id;
	@ManyToOne(targetEntity = LicitacaoModalidade.class)
	@JoinColumn(name = "FK_LICITACAO_MODALIDADE", nullable = false, foreignKey = @ForeignKey(name = "FK_LICITACAO_MODALIDADE"))
	private LicitacaoModalidade licitacaoModalidade;
	@ManyToOne(targetEntity = Competencia.class)
	@JoinColumn(name = "FK_COMPETENCIA", nullable = false, foreignKey = @ForeignKey(name = "FK_COMPETENCIA"))
	private Competencia competencia;
	@ManyToOne(targetEntity = Entidade.class)
	@JoinColumn(name = "FK_ENTIDADE", nullable = false, foreignKey = @ForeignKey(name = "FK_ENTIDADE"))
	private Entidade entidade;
	@Column(name = "NR_NUMERO", nullable = false)
	private Integer numero;
	@Column(name = "NR_ANO", nullable = false)
	private Integer ano;
	@Column(name = "VR_VALOR_ESTIMADO", nullable = false, length = 25, precision = 2)
	private BigDecimal valorEstimado;
	@Temporal(TIMESTAMP)
	@Column(name = "TS_DATA_HORA_MARCADA", nullable = false)
	private Date dataHoraMarcada;
	@Enumerated(STRING)
	@Column(name = "ST_REGIME", nullable = false, length = 20)
	private LicitacaoRegimeEnum regime;
	@Enumerated(STRING)
	@Column(name = "ST_HOMOLOGOU", nullable = false, length = 3)
	private SimNaoEnum homologou;
	@Enumerated(STRING)
	@Column(name = "ST_TERMINOU", nullable = false, length = 3)
	private SimNaoEnum terminou;
	@OneToMany(mappedBy = "licitacao", targetEntity = LicitacaoItem.class)
	private Collection<LicitacaoItem> licitacaoItens;
	@OneToOne(mappedBy = "licitacao", fetch = LAZY, targetEntity = Pregao.class)
	private Pregao pregao;
}
