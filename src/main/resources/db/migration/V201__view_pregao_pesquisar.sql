CREATE OR REPLACE VIEW VIEW_PREGAO_PESQUISAR AS
SELECT

	--pregao
	PR.PK_PREGAO,
	PR.ST_FASE,
	PR.TS_DATA_HORA_FECHOU,
	PR.TS_DATA_HORA_ABERTURA,
	PR.ST_REGISTRO,
	PR.VR_MINIMO_POR_LANCE,
	PR.NR_QUANTIDADE_FORNECEDOR,
	--Competencia
	MA.PK_COMPETENCIA,
	MA.NR_ANO COMPETENCIA_ANO,
	MA.ST_MES,
	--Licitacao
	LI.PK_LICITACAO,
	LI.NR_NUMERO,
	LI.NR_ANO LICITACAO_ANO,
	LI.VR_VALOR_ESTIMADO,
	LI.ST_REGIME,
	LI.FK_ENTIDADE,
	--Licitacao item
	(SELECT COUNT(LIN.FK_LICITACAO) FROM TAB_LICITACAO_ITEM LIN WHERE LIN.FK_LICITACAO = LI.PK_LICITACAO) NR_QTD_ITENS

FROM TAB_PREGAO PR
INNER JOIN TAB_LICITACAO LI ON PR.FK_LICITACAO = LI.PK_LICITACAO
INNER JOIN TAB_COMPETENCIA MA ON PR.FK_COMPETENCIA = MA.PK_COMPETENCIA;
