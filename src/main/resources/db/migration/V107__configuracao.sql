CREATE SEQUENCE SQ_CONFIGURACAO START 1 INCREMENT 1;

CREATE TABLE IF NOT EXISTS TAB_CONFIGURACAO (
  PK_CONFIGURACAO BIGINT NOT NULL,
  ST_SITUACAO VARCHAR(15) NOT NULL,
  TS_DATA_HORA_CADASTRO TIMESTAMP WITH TIME ZONE NOT NULL,
  NR_VERSAO INTEGER NOT NULL,
  ST_REGISTRO VARCHAR(15) NOT NULL,
  TS_MOVIMENTACAO TIMESTAMP WITH TIME ZONE NOT NULL,
  CD_LOGIN_MOVIMENTACAO VARCHAR(300) NOT NULL,
  IP_MOVIMENTACAO VARCHAR(40) NOT NULL,
  TP_OPERACAO VARCHAR(10) NOT NULL,
  FK_ENTIDADE BIGINT NOT NULL,
  VR_PORCENTAGEM_MP_EPP NUMERIC(25,2) NOT NULL,
  NR_TEMPO_MP_EPP INTEGER NOT NULL,
  NR_QUANTIDADE_FORNECEDORES INTEGER NOT NULL,
  ST_BANCO_LOCAL VARCHAR(3) NOT NULL,
  CONSTRAINT TAB_CONFIGURACAO_KEY PRIMARY KEY(PK_CONFIGURACAO)
) 
WITH (oids = false);

ALTER TABLE IF EXISTS TAB_CONFIGURACAO ADD CONSTRAINT FK_ENTIDADE FOREIGN KEY (FK_ENTIDADE) REFERENCES TAB_ENTIDADE;

CREATE INDEX I1_TAB_CONFIGURACAO on TAB_CONFIGURACAO (ST_REGISTRO);
CREATE INDEX I2_TAB_CONFIGURACAO on TAB_CONFIGURACAO (FK_ENTIDADE);
CREATE INDEX I3_TAB_CONFIGURACAO on TAB_CONFIGURACAO (ST_BANCO_LOCAL);

ALTER TABLE IF EXISTS TAB_CONFIGURACAO ADD CONSTRAINT U1_TAB_CONFIGURACAO UNIQUE (FK_ENTIDADE);

INSERT INTO TAB_CONFIGURACAO (PK_CONFIGURACAO, ST_SITUACAO, TS_DATA_HORA_CADASTRO, NR_VERSAO, ST_REGISTRO, TS_MOVIMENTACAO,
CD_LOGIN_MOVIMENTACAO, IP_MOVIMENTACAO, TP_OPERACAO, FK_ENTIDADE, VR_PORCENTAGEM_MP_EPP, NR_TEMPO_MP_EPP, 
NR_QUANTIDADE_FORNECEDORES, ST_BANCO_LOCAL) 
VALUES (nextval('SQ_CONFIGURACAO'), 'EM_EDICAO', now(), 0, 'ATIVO', now(), 'CARGA INICIAL', 'CARGA INICIAL', 'INCLUSAO', 
(SELECT PK_EMPRESA FROM TAB_ENTIDADE WHERE DS_CNPJ = '15056724000124'), 5, 30, 3, 'SIM');

INSERT INTO TAB_CONFIGURACAO (PK_CONFIGURACAO, ST_SITUACAO, TS_DATA_HORA_CADASTRO, NR_VERSAO, ST_REGISTRO, TS_MOVIMENTACAO,
CD_LOGIN_MOVIMENTACAO, IP_MOVIMENTACAO, TP_OPERACAO, FK_ENTIDADE, VR_PORCENTAGEM_MP_EPP, NR_TEMPO_MP_EPP, 
NR_QUANTIDADE_FORNECEDORES, ST_BANCO_LOCAL)
VALUES (nextval('SQ_CONFIGURACAO'), 'EM_EDICAO', now(), 0, 'ATIVO', now(), 'CARGA INICIAL', 'CARGA INICIAL', 'INCLUSAO', 
(SELECT PK_EMPRESA FROM TAB_ENTIDADE WHERE DS_CNPJ = '93539867000109'), 5, 30, 3, 'SIM');