CREATE SEQUENCE SQ_MICRO_EPP_EMPRESA START 1 INCREMENT 1;

CREATE TABLE IF NOT EXISTS TAB_MICRO_EPP_EMPRESA (
  PK_MICRO_EPP_EMPRESA BIGINT NOT NULL,
  ST_SITUACAO VARCHAR(15) NOT NULL,
  TS_DATA_HORA_CADASTRO TIMESTAMP WITH TIME ZONE NOT NULL,
  NR_VERSAO INTEGER NOT NULL,
  ST_REGISTRO VARCHAR(15) NOT NULL,
  TS_MOVIMENTACAO TIMESTAMP WITH TIME ZONE NOT NULL,
  CD_LOGIN_MOVIMENTACAO VARCHAR(300) NOT NULL,
  IP_MOVIMENTACAO VARCHAR(40) NOT NULL,
  TP_OPERACAO VARCHAR(10) NOT NULL,
  FK_PREGAO BIGINT NOT NULL,
  FK_PREGAO_ITEM BIGINT,
  FK_PESSOA BIGINT NOT NULL,
  VR_ATUAL NUMERIC(25,2) NOT NULL,
  VR_ULTIMO NUMERIC(25,2) NOT NULL,
  CONSTRAINT TAB_MICRO_EPP_EMPRESA_KEY PRIMARY KEY(PK_MICRO_EPP_EMPRESA)
) 
WITH (oids = false);

ALTER TABLE IF EXISTS TAB_MICRO_EPP_EMPRESA ADD CONSTRAINT FK_PREGAO_ITEM FOREIGN KEY (FK_PREGAO_ITEM) REFERENCES TAB_PREGAO_ITEM;
ALTER TABLE IF EXISTS TAB_MICRO_EPP_EMPRESA ADD CONSTRAINT FK_PESSOA FOREIGN KEY (FK_PESSOA) REFERENCES TAB_PESSOA;
ALTER TABLE IF EXISTS TAB_MICRO_EPP_EMPRESA ADD CONSTRAINT FK_PREGAO FOREIGN KEY (FK_PREGAO) REFERENCES TAB_PREGAO;

CREATE INDEX I1_TAB_MICRO_EPP_EMPRESA on TAB_MICRO_EPP_EMPRESA (ST_REGISTRO);
CREATE INDEX I2_TAB_MICRO_EPP_EMPRESA on TAB_MICRO_EPP_EMPRESA (FK_PREGAO);
CREATE INDEX I3_TAB_MICRO_EPP_EMPRESA on TAB_MICRO_EPP_EMPRESA (FK_PREGAO_ITEM);
CREATE INDEX I4_TAB_MICRO_EPP_EMPRESA on TAB_MICRO_EPP_EMPRESA (FK_PESSOA);

--Para itens
ALTER TABLE IF EXISTS TAB_MICRO_EPP_EMPRESA ADD CONSTRAINT U1_TAB_MICRO_EPP_EMPRESA UNIQUE (FK_PESSOA, FK_PREGAO, FK_PREGAO_ITEM, VR_ATUAL, VR_ULTIMO);
--Para pregao
ALTER TABLE IF EXISTS TAB_MICRO_EPP_EMPRESA ADD CONSTRAINT U2_TAB_MICRO_EPP_EMPRESA UNIQUE (FK_PESSOA, FK_PREGAO, VR_ATUAL, VR_ULTIMO);
